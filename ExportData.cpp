// PlotSelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DBFunc.h"
#include "UMRojning.h"
#include "ExportData.h"


/////////////////////////////////////////////////////////////////////////////
// CExportDataView

IMPLEMENT_DYNCREATE(CExportDataView, CFormView)

BEGIN_MESSAGE_MAP(CExportDataView, CFormView)
	//{{AFX_MSG_MAP(CExportDataView)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CExportDataView::CExportDataView():
CFormView(CExportDataView::IDD),
m_pDB(NULL)
{
}

CExportDataView::~CExportDataView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CExportDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void CExportDataView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));
}

void CExportDataView::ShowExportDialog()
{
	CFileDialog *pFileDlg;

	if( !(pFileDlg = new CFileDialog(FALSE, _T("*.mdb"), NULL, 0, _T("Access-databaser (*.mdb)|*.mdb|Alla filer (*.*)|*.*||"))) )
	{
		return;
	}

	// Show export dialog
	if( pFileDlg->DoModal() == IDOK )
	{
		if( m_pDB->ExportRecords(pFileDlg->GetPathName()) )
		{
			AfxMessageBox(_T("Exporteringen lyckades!"), MB_ICONINFORMATION | MB_OK);
		}
	}
}


// CExportDataView message handlers

BOOL CExportDataView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}

		// Bring up export dialog when db connection has been set up
		if( m_pDB )
		{
			ShowExportDialog();
		}

		// Close this view so user can bring up this dialog again
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CExportDataView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}



/////////////////////////////////////////////////////////////////////////////
// CExportDataFrame

IMPLEMENT_DYNCREATE(CExportDataFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CExportDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CExportDataFrame)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExportDataFrame::CExportDataFrame()
{
}

CExportDataFrame::~CExportDataFrame()
{
}

void CExportDataFrame::ActivateFrame(int nCmdShow)
{
	// HACKHACK: hide this window
	CMDIChildWnd::ActivateFrame(SW_HIDE);
}

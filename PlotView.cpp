#include "stdafx.h"
#include "DBFunc.h"
#include "UMRojningGenerics.h"
#include "UMRojningStrings.h"
#include "PlotSelectList.h"
#include "PlotView.h"


// CPlotView

IMPLEMENT_DYNCREATE(CPlotView, CFormView)

BEGIN_MESSAGE_MAP(CPlotView, CFormView)
	//{{AFX_MSG_MAP(CPlotView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPlotView::CPlotView():
CFormView(CPlotView::IDD),
m_pDB(NULL),
m_nDBIndex(0)
{
}

CPlotView::~CPlotView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CPlotView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT7, m_region);
	DDX_Control(pDX, IDC_EDIT10, m_distrikt);
	DDX_Control(pDX, IDC_EDIT6, m_lag);
	DDX_Control(pDX, IDC_EDIT8, m_karta);
	DDX_Control(pDX, IDC_EDIT12, m_bestand);
	DDX_Control(pDX, IDC_EDIT13, m_delbestand);
	DDX_Control(pDX, IDC_EDIT11, m_ytnr);
	DDX_Control(pDX, IDC_EDIT24, m_latitud);
	DDX_Control(pDX, IDC_EDIT25, m_longitud);
	DDX_Control(pDX, IDC_COMBO19, m_hansynsyta);
	DDX_Control(pDX, IDC_COMBO17, m_orojt);
	DDX_Control(pDX, IDC_COMBO18, m_lagskarm);
	DDX_Control(pDX, IDC_EDIT27, m_totalalder);
	DDX_Control(pDX, IDC_COMBO9, m_vegtyp);
	DDX_Control(pDX, IDC_COMBO10, m_jordart);
	DDX_Control(pDX, IDC_COMBO11, m_markfukt);
	DDX_Control(pDX, IDC_EDIT22, m_si);
	DDX_Control(pDX, IDC_COMBO33, m_kommentar);
	DDX_Control(pDX, IDC_COMBO34, m_rattTrsl);
	DDX_Control(pDX, IDC_COMBO35, m_nastaAtgard);
	DDX_Control(pDX, IDC_EDIT35, m_stam[0]);
	DDX_Control(pDX, IDC_EDIT36, m_stam[1]);
	DDX_Control(pDX, IDC_EDIT37, m_stam[2]);
	DDX_Control(pDX, IDC_EDIT38, m_stam[3]);
	DDX_Control(pDX, IDC_EDIT39, m_stam[4]);
	DDX_Control(pDX, IDC_EDIT40, m_betadStam[0]);
	DDX_Control(pDX, IDC_EDIT41, m_betadStam[1]);
	DDX_Control(pDX, IDC_EDIT42, m_betadStam[2]);
	DDX_Control(pDX, IDC_EDIT43, m_betadStam[3]);
	DDX_Control(pDX, IDC_EDIT44, m_betadStam[4]);
	DDX_Control(pDX, IDC_EDIT45, m_aterbetadStam[0]);
	DDX_Control(pDX, IDC_EDIT46, m_aterbetadStam[1]);
	DDX_Control(pDX, IDC_EDIT47, m_aterbetadStam[2]);
	DDX_Control(pDX, IDC_EDIT48, m_aterbetadStam[3]);
	DDX_Control(pDX, IDC_EDIT49, m_aterbetadStam[4]);
	DDX_Control(pDX, IDC_EDIT50, m_stamMhojd[0]);
	DDX_Control(pDX, IDC_EDIT51, m_stamMhojd[1]);
	DDX_Control(pDX, IDC_EDIT52, m_stamMhojd[2]);
	DDX_Control(pDX, IDC_EDIT53, m_stamMhojd[3]);
	DDX_Control(pDX, IDC_EDIT54, m_stamMhojd[4]);
}

void CPlotView::PopulateData(UINT idx /*=0*/)
{
	int i;
	CString str, tmp;
	PLOTHEADER *ph;

	// Update index if specified, just to first plant
	if( idx )
	{
		m_nDBIndex = idx;
		m_nPlantIndex = 0;
	}

	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_plotData.size() )
	{
		ph = &m_plotData[m_nDBIndex];

		// Load cached data
		m_pDB->GetRegionName(ph->region, tmp);
		str.Format(_T("%d - %s"), ph->region, tmp);
		m_region.SetWindowText(str);

		tmp.Empty();
		m_pDB->GetDistrictName(ph->distrikt, ph->region, tmp);
		str.Format(_T("%d - %s"), ph->distrikt, tmp);
		m_distrikt.SetWindowText(str);

		tmp.Empty();
		m_pDB->GetTeamName(ph->lag, ph->distrikt, ph->region, tmp);
		str.Format(_T("%d - %s"), ph->lag, tmp);
		m_lag.SetWindowText(str);

		m_karta.setInt(ph->karta);
		m_bestand.setInt(ph->bestand);
		if( ph->delbestand > 0 )
		{
			m_delbestand.setInt(ph->delbestand);
		}
		else if( ph->delbestand == 0 )
		{
			m_delbestand.SetWindowText(_T("0"));
		}
		else
		{
			m_delbestand.SetWindowText(_T(""));
		}
		m_ytnr.setInt(ph->ytnr);

		m_latitud.SetWindowText(ph->latitud);
		m_longitud.SetWindowText(ph->longitud);
		m_hansynsyta.SetCurSel(ph->hansynsyta);
		m_orojt.SetCurSel(ph->orojt);
		m_lagskarm.SetCurSel(ph->lagskarm);
		m_totalalder.setInt(ph->totalalder);
		m_vegtyp.SetCurSel(ph->vegtyp);
		m_markfukt.SetCurSel(MAX(ph->markfukt - 1, -1));
		m_si.SetWindowText(ph->si);
		m_kommentar.SetCurSel(ph->kommentar);
		m_rattTrsl.SetCurSel(ph->rattTrsl);
		m_nastaAtgard.SetCurSel(ph->nastaAtgard);

		if( ph->vegtyp == -1 )
		{
			m_vegtyp.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < VEGTYP_CT; i++ )
			{
				if( ph->vegtyp == VEGTYP_INDICES[i] )
				{
					m_vegtyp.SetCurSel(i);
				}
			}
		}

		if( ph->jordart == -1 )
		{
			m_jordart.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < JORDART_CT; i++ )
			{
				if( ph->jordart == JORDART_INDICES[i] )
				{
					m_jordart.SetCurSel(i);
				}
			}
		}

		for( i = 0; i < 5; i++ )
		{
			m_stam[i].setInt(ph->stam[i]);
			m_betadStam[i].setInt(ph->betadStam[i]);
			m_aterbetadStam[i].setInt(ph->aterbetadStam[i]);
			m_stamMhojd[i].setInt(ph->stamMhojd[i]);
		}
	}
	else
	{
		// Clear fields
		m_region.SetWindowText(_T(""));
		m_distrikt.SetWindowText(_T(""));
		m_lag.SetWindowText(_T(""));
		m_karta.SetWindowText(_T(""));
		m_ytnr.SetWindowText(_T(""));
		m_latitud.SetWindowText(_T(""));
		m_longitud.SetWindowText(_T(""));
		m_hansynsyta.SetCurSel(-1);
		m_orojt.SetCurSel(-1);
		m_lagskarm.SetCurSel(-1);
		m_totalalder.SetWindowText(_T(""));
		m_vegtyp.SetCurSel(-1);
		m_jordart.SetCurSel(-1);
		m_markfukt.SetCurSel(-1);
		m_si.SetWindowText(_T(""));
		m_kommentar.SetCurSel(-1);
		m_rattTrsl.SetCurSel(-1);
		m_nastaAtgard.SetCurSel(-1);
		for( i = 0; i < 5; i++ )
		{
			m_stam[i].SetWindowText(_T(""));
			m_betadStam[i].SetWindowText(_T(""));
			m_aterbetadStam[i].SetWindowText(_T(""));
			m_stamMhojd[i].SetWindowText(_T(""));
		}
	}
}

void CPlotView::DeletePlot()
{
	CString msg;
	PLOTHEADER &ph = m_plotData[m_nDBIndex];

	// Make sure record exist
	if( m_nDBIndex >= (int)m_plotData.size() )
		return;

	// Bring up confirm dialog
	msg.Format( _T("<center><font size=\"+4\"><b>Ta bort yta</b></font></center><br><hr><br>")
				_T("Ytnr: <b>%d</b><br><hr><br>")
				_T("<center><font color=\"red\" size=\"+4\">Vill du ta bort denna yta?</font></center>"),
				ph.ytnr );
	if( messageDialog(_T("Ta bort yta"), _T("Ta bort"), _T("Avbryt"), msg) )
	{
		// OK - go on remove plot
		if( m_pDB->DeletePlot(ph) )
		{
			ReloadPlotData();
		}
	}
}

void CPlotView::ReloadPlotData(bool bUpdateToolbar /*=true*/)
{
	if( !m_pDB ) return;

	// Update plot data
	m_pDB->GetPlotData( m_plotData );

	// Just to last record if we're out of bounds
	if( m_nDBIndex >= (int)m_plotData.size() )
		m_nDBIndex = max(0, (int)m_plotData.size() - 1);
	
	PopulateData(m_nDBIndex);

	// Update toolbar only if specified, we don't want to interfere with TraktView
	if( bUpdateToolbar )
		SetupToolbarButtons();


	// Update plot list view
	CPlotSelectList *pPSL = (CPlotSelectList*)getFormViewByID( IDD_PLOTSELECT );
	if( pPSL )
	{
		pPSL->PopulateReport();
	}
}

void CPlotView::SetupToolbarButtons()
{
	BOOL startPrev, endNext;

	// Enable/disable db navig buttons depending on where in the list we are
	if( m_plotData.size() <= 1 )
	{
		// Only one record or less
		startPrev = FALSE;
		endNext = FALSE;
	}
	else if( m_nDBIndex <= 0 )
	{
		// At the beginning
		startPrev = FALSE;
		endNext = TRUE;
	}
	else if( m_nDBIndex >= ((int)m_plotData.size() - 1) )
	{
		// At the end
		startPrev = TRUE;
		endNext = FALSE;
	}
	else
	{
		// In the middle somewhere
		startPrev = TRUE;
		endNext = TRUE;
	}

	// DB navigation
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	// Tools
	BOOL deleteItem = m_plotData.size() ? TRUE : FALSE; // enable only if we have any records
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,deleteItem);
}

void CPlotView::OnInitialUpdate()
{
	int i;

	CFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	// Check if we have a valid license
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H9002"), _T(""), _T(""), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);
	if( _tcscmp(msg.getSuite(), _T("0")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}


	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();

	// Fill up combo boxes with possible values
	for( i = 0; i < VEGTYP_CT; i++ )
	{
		m_vegtyp.AddString(VEGTYP[i]);
	}
	for( i = 0; i < JORDART_CT; i++ )
	{
		m_jordart.AddString(JORDART[i]);
	}
	for( i = 0; i < MARKFUKT_CT; i++ )
	{
		m_markfukt.AddString(MARKFUKT[i]);
	}
	for( i = 0; i < KOMMENTARANTSTAM_CT; i++ )
	{
		m_kommentar.AddString(KOMMENTARANTSTAM[i]);
	}
	for( i = 0; i < RATTTRADSLAG_CT; i++ )
	{
		m_rattTrsl.AddString(RATTTRADSLAG[i]);
	}
	for( i = 0; i < NASTAATGARD_CT; i++ )
	{
		m_nastaAtgard.AddString(NASTAATGARD[i]);
	}
	for( i = 0; i < NEJJA_CT; i++ )
	{
		m_hansynsyta.AddString(NEJJA[i]);
		m_orojt.AddString(NEJJA[i]);
		m_lagskarm.AddString(NEJJA[i]);
	}

	// Set fields to read-only
	m_region.SetDisabledColor(BLACK, INFOBK);
	m_distrikt.SetDisabledColor(BLACK, INFOBK);
	m_lag.SetDisabledColor(BLACK, INFOBK);
	m_karta.SetDisabledColor(BLACK, INFOBK);
	m_ytnr.SetDisabledColor(BLACK, INFOBK);
	m_latitud.SetDisabledColor(BLACK, INFOBK);
	m_longitud.SetDisabledColor(BLACK, INFOBK);
	m_hansynsyta.SetDisabledColor(BLACK, INFOBK);
	m_orojt.SetDisabledColor(BLACK, INFOBK);
	m_lagskarm.SetDisabledColor(BLACK, INFOBK);
	m_totalalder.SetDisabledColor(BLACK, INFOBK);
	m_vegtyp.SetDisabledColor(BLACK, INFOBK);
	m_jordart.SetDisabledColor(BLACK, INFOBK);
	m_markfukt.SetDisabledColor(BLACK, INFOBK);
	m_si.SetDisabledColor(BLACK, INFOBK);
	m_kommentar.SetDisabledColor(BLACK, INFOBK);
	m_rattTrsl.SetDisabledColor(BLACK, INFOBK);
	m_nastaAtgard.SetDisabledColor(BLACK, INFOBK);
	for( i = 0; i < 5; i++ )
	{
		m_stam[i].SetDisabledColor(BLACK, INFOBK);
		m_betadStam[i].SetDisabledColor(BLACK, INFOBK);
		m_aterbetadStam[i].SetDisabledColor(BLACK, INFOBK);
		m_stamMhojd[i].SetDisabledColor(BLACK, INFOBK);
	}

	m_region.SetReadOnly();
	m_distrikt.SetReadOnly();
	m_lag.SetReadOnly();
	m_karta.SetReadOnly();
	m_ytnr.SetReadOnly();
	m_latitud.SetReadOnly();
	m_longitud.SetReadOnly();
	m_hansynsyta.SetReadOnly();
	m_orojt.SetReadOnly();
	m_lagskarm.SetReadOnly();
	m_totalalder.SetReadOnly();
	m_vegtyp.SetReadOnly();
	m_jordart.SetReadOnly();
	m_markfukt.SetReadOnly();
	m_si.SetReadOnly();
	m_kommentar.SetReadOnly();
	m_rattTrsl.SetReadOnly();
	m_nastaAtgard.SetReadOnly();
	for( i = 0; i < 5; i++ )
	{
		m_stam[i].SetReadOnly();
		m_betadStam[i].SetReadOnly();
		m_aterbetadStam[i].SetReadOnly();
		m_stamMhojd[i].SetReadOnly();
	}

	// Cache plot data from db
	if( m_pDB )
	{
		m_pDB->GetPlotData( m_plotData );
	}

	PopulateData(m_nDBIndex);
}

BOOL CPlotView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CPlotView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}

LRESULT CPlotView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		m_pDB->ShowImportDialog();
		break;

	case ID_DELETE_ITEM:
		DeletePlot();
		break;

	case ID_DBNAVIG_START:
		m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_PREV:
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_NEXT:
		if( ++m_nDBIndex > ((int)m_plotData.size() - 1) )
			m_nDBIndex = (int)m_plotData.size() - 1;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_END:
		m_nDBIndex = ((int)m_plotData.size() - 1);

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;
	};

	return 0;
}

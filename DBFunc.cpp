#include "stdafx.h"

#ifdef UNICODE
	#undef UNICODE
	#include "tinyxml.h"
	#define UNICODE
#else
	#include "tinyxml.h"
#endif

#include <DBBaseClass_SQLApi.h>
#include <shlobj.h>
#include <SQLAPI.h>
#include "DBFunc.h"

const TCHAR SUBDIR_HMS[]		= _T("HMS");
const TCHAR SUBDIR_DATA[]		= _T("Data");
const TCHAR SUBDIR_R�JNING[]	= _T("R�jning");


const TCHAR SQLCREATE_TRAKT[] =	_T("CREATE TABLE [dbo].[rojning_trakt]")
								_T("(")
									_T("[region] int NOT NULL,")
									_T("[distrikt] int NOT NULL,")
									_T("[lag] int NOT NULL,")
									_T("[karta] int NOT NULL,")
									_T("[bestand] smallint NOT NULL,")
									_T("[delbestand] smallint NOT NULL,")
									_T("[invtyp] smallint NOT NULL,")
									_T("[ursprung] smallint NOT NULL,")
									_T("[anvandare] varchar(50) NULL,")
									_T("[datum] datetime NOT NULL,")
									_T("[namn] varchar(50) NULL,")
									_T("[areal] float NULL,")
									_T("[hoh] smallint NOT NULL,")
									_T("[radie] float NOT NULL,")
									_T("[stamTall] smallint NULL,")
									_T("[stamGran] smallint NULL,")
									_T("[stamBjork] smallint NULL,")
									_T("[stamOvlov] smallint NULL,")
									_T("[stamCont] smallint NULL,")
									_T("[stamMhojdTall] smallint NULL,")
									_T("[stamMhojdGran] smallint NULL,")
									_T("[stamMhojdBjork] smallint NULL,")
									_T("[stamMhojdOvlov] smallint NULL,")
									_T("[stamMhojdCont] smallint NULL,")
									_T("[totalalder] smallint NULL,")
									_T("[vegtyp] smallint NULL,")
									_T("[jordart] smallint NULL,")
									_T("[markfukt] smallint NULL,")
									_T("[si] varchar(5) NULL,")
									_T("[avvikerFranRUS] smallint NULL,")
									_T("[avvikerKommentar] varchar(200) NULL,")
									_T("[myr] smallint NULL,")
									_T("[hallmark] smallint NULL,")
									_T("[hansynBiotop] smallint NULL,")
									_T("[lovandel] smallint NULL,")
									_T("[kulturmiljoer] smallint NULL,")
									_T("[fornminnen] smallint NULL,")
									_T("[skyddszoner] smallint NULL,")
									_T("[naturvardestrad] smallint NULL,")
									_T("[nedskrapning] smallint NULL,")
									_T("[fritext] varchar(200) NULL,")
									_T("CONSTRAINT [PK_rojning_trakt] PRIMARY KEY")
									_T("(")
										_T("[region],")
										_T("[distrikt],")
										_T("[lag],")
										_T("[karta],")
										_T("[bestand],")
										_T("[delbestand]")
									_T(")")
								_T(")");

const TCHAR SQLCREATE_YTA[] =	_T("CREATE TABLE [dbo].[rojning_yta]")
								_T("(")
									_T("[ytnr] smallint NOT NULL,")
									_T("[region] int NOT NULL,")
									_T("[distrikt] int NOT NULL,")
									_T("[lag] int NOT NULL,")
									_T("[karta] int NOT NULL,")
									_T("[bestand] smallint NOT NULL,")
									_T("[delbestand] smallint NOT NULL,")
									_T("[latitud] varchar(10) NULL,")
									_T("[longitud] varchar(10) NULL,")
									_T("[hansynsyta] smallint NULL,")
									_T("[orojt] smallint NULL,")
									_T("[lagskarm] smallint NULL,")
									_T("[totalalder] smallint NULL,")
									_T("[vegtyp] smallint NULL,")
									_T("[jordart] smallint NULL,")
									_T("[markfukt] smallint NULL,")
									_T("[si] varchar(5) NULL,")
									_T("[stamTall] smallint NULL,")
									_T("[stamGran] smallint NULL,")
									_T("[stamBjork] smallint NULL,")
									_T("[stamOvlov] smallint NULL,")
									_T("[stamCont] smallint NULL,")
									_T("[betadStamTall] smallint NULL,")
									_T("[betadStamGran] smallint NULL,")
									_T("[betadStamBjork] smallint NULL,")
									_T("[betadStamOvlov] smallint NULL,")
									_T("[betadStamCont] smallint NULL,")
									_T("[aterbetadStamTall] smallint NULL,")
									_T("[aterbetadStamGran] smallint NULL,")
									_T("[aterbetadStamBjork] smallint NULL,")
									_T("[aterbetadStamOvlov] smallint NULL,")
									_T("[aterbetadStamCont] smallint NULL,")
									_T("[stamMhojdTall] smallint NULL,")
									_T("[stamMhojdGran] smallint NULL,")
									_T("[stamMhojdBjork] smallint NULL,")
									_T("[stamMhojdOvlov] smallint NULL,")
									_T("[stamMhojdCont] smallint NULL,")
									_T("[kommentar] smallint NULL,")
									_T("[rattTrsl] smallint NULL,")
									_T("[nastaAtgard] smallint NULL,")
									_T("CONSTRAINT [PK_rojning_yta] PRIMARY KEY")
									_T("(")
										_T("[ytnr],")
										_T("[region],")
										_T("[distrikt],")
										_T("[lag],")
										_T("[karta],")
										_T("[bestand],")
										_T("[delbestand]")
									_T(")")
								_T(")");

const TCHAR SQLCONSTRAINT_YTA[]	=	_T("ALTER TABLE [rojning_yta] ADD CONSTRAINT [FK_rojning_yta_rojning_trakt] FOREIGN KEY([region], [distrikt], [lag], [karta], [bestand], [delbestand]) ")
									_T("REFERENCES [rojning_trakt] ([region], [distrikt], [lag], [karta], [bestand], [delbestand]) ")
									_T("ON UPDATE CASCADE ")
									_T("ON DELETE CASCADE");

const TCHAR SQLVIEW_ROJNING[] =	_T("CREATE VIEW dbo.rojning AS ")
								_T("SELECT * FROM ")
								_T("( ")
								_T("SELECT antalTall, antalGran, antalBjork, antalOvlov, antalCont, hojdTall, hojdGran, hojdBjork, hojdOvlov, hojdCont, antalTotalt, r, areal a, radie*radie*PI() ytAreal, orojt, region rg, distrikt di, lag lg, karta ka, bestand bd, delbestand db, traktnamn, ")
										_T("ROUND(CAST((antalBjork + antalOvlov) AS float) / (antalTall + antalGran + antalBjork + antalOvlov + antalCont) * 100, 0) la, ")
										_T("s, ")
										_T("(SELECT COUNT(*) FROM rojning_yta y WHERE y.region = dt.region AND y.distrikt = dt.distrikt AND y.lag = dt.lag AND y.karta = dt.karta AND y.bestand = dt.bestand AND y.delbestand = dt.delbestand AND y.orojt = dt.orojt) antalYtor, ")
										_T("(SELECT SUM(CAST( ")
										_T("CASE WHEN ")
											_T("((y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) / (radie*radie*PI()) * 10000) > s - 500 ")
											_T("AND ")
											_T("((y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) / (radie*radie*PI()) * 10000) < s + 500 ")
										_T("THEN ")
											_T("1 ")
										_T("ELSE ")
											_T("0 ")
										_T("END ")
										_T("AS float)) ")
										_T("FROM (SELECT y.stamTall, y.stamGran, y.stamBjork, y.stamOvlov, y.stamCont, radie, ")
													_T("CASE WHEN (SELECT ROUND(CAST(SUM(stamBjork + stamOvlov) AS float) / SUM(stamTall + stamGran + stamBjork + stamOvlov + stamCont) * 100, 0) FROM rojning_yta WHERE region = y.region AND distrikt = y.distrikt AND lag = y.lag AND karta = y.karta AND bestand = y.bestand AND delbestand = y.delbestand) > 69 THEN ")
													_T("( ")
														_T("CASE ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 1300 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 1400 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 1500 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 1600 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 1700 ")
														_T("END")
													_T(") ")
													_T("ELSE ")
													_T("( ")
														_T("CASE ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 2100 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 2200 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 2300 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 2400 ")
														_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 2500 ")
														_T("END")
													_T(") ")
													_T("END s ")
										_T("FROM rojning_yta y INNER JOIN rojning_trakt t ON t.region = y.region AND t.distrikt = y.distrikt AND t.lag = y.lag AND t.karta = y.karta AND t.bestand = y.bestand AND t.delbestand = y.delbestand ")
										_T("WHERE y.region = dt.region AND y.distrikt = dt.distrikt AND y.lag = dt.lag AND y.karta = dt.karta AND y.bestand = dt.bestand AND y.delbestand = dt.delbestand AND y.orojt = dt.orojt) y) antalGodkanda ")
								_T("FROM ")
								_T("( ")
									_T("SELECT ")
									_T("CASE WHEN (SELECT ROUND(CAST(SUM(stamBjork + stamOvlov) AS float) / SUM(stamTall + stamGran + stamBjork + stamOvlov + stamCont) * 100, 0) FROM rojning_yta WHERE region = y.region AND distrikt = y.distrikt AND lag = y.lag AND karta = y.karta AND bestand = y.bestand AND delbestand = y.delbestand) > 69 THEN ")
									_T("( ")
										_T("CASE ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 1300 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 1400 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 1500 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 1600 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 1700 ")
										_T("END")
									_T(") ")
									_T("ELSE")
									_T("( ")
										_T("CASE ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 2100 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 2200 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 2300 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 2400 ")
										_T("WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 2500 ")
										_T("END")
									_T(") ") 
									_T("END s, ")
									_T("SUM(y.stamTall) antalTall, SUM(y.stamMhojdTall * y.stamTall) hojdTall, ")
									_T("SUM(y.stamGran) antalGran, SUM(y.stamMhojdGran * y.stamGran) hojdGran, ")
									_T("SUM(y.stamBjork) antalBjork, SUM(y.stamMhojdBjork * y.stamBjork) hojdBjork, ")
									_T("SUM(y.stamOvlov) antalOvlov, SUM(y.stamMhojdOvlov * y.stamOvlov) hojdOvlov, ")
									_T("SUM(y.stamCont) antalCont, SUM(y.stamMhojdCont * y.stamCont) hojdCont, ")
									_T("SUM(y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) antalTotalt, ")
									_T("CASE WHEN (y.region = 11 OR y.region = 16) AND y.distrikt <> 21 THEN 5 ELSE 10 END r, ")
									_T("(t.areal / (SELECT COUNT(*) FROM rojning_yta y1 WHERE y1.region = y.region AND y1.distrikt = y.distrikt AND y1.lag = y.lag AND y1.karta = y.karta AND y1.bestand = y.bestand AND y1.delbestand = y.delbestand) * COUNT(*)) areal, ")
									_T("radie, ")
									_T("orojt, y.region, y.distrikt, y.lag, y.karta, y.bestand, y.delbestand, ")
									_T("namn traktnamn ")
									_T("FROM rojning_yta AS y ")
									_T("INNER JOIN ")
									_T("rojning_trakt AS t ON t.region = y.region AND t.distrikt = y.distrikt AND t.lag = y.lag AND t.karta = y.karta AND t.bestand = y.bestand AND t.delbestand = y.delbestand ")
									_T("GROUP BY y.region, y.distrikt, y.lag, y.karta, y.bestand, y.delbestand, namn, orojt, areal, radie, t.si ")
								_T(") dt ")
								_T(") dt2 ")
								_T("INNER JOIN rojning_trakt AS t ON t.region = rg AND t.distrikt = di AND t.lag = lg AND t.karta = ka AND t.bestand = bd AND t.delbestand = db");


CDBFunc::CDBFunc( DB_CONNECTION_DATA &condata )
	: CDBBaseClass_SQLApi(condata, 1)
{}

CDBFunc::~CDBFunc()
{}

void CDBFunc::setupForDBConnection( HWND hWndReciv, HWND hWndSend )
{
	// Request connection info from shell
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;

		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv, WM_COPYDATA, (WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

bool CDBFunc::DeletePlot(PLOTHEADER &ph)
{
	// Remove plot record from db
	try
	{
		m_saCommand.setCommandText(_T("DELETE FROM rojning_yta WHERE ytnr = :1 AND region = :2 AND distrikt = :3 AND lag = :4 AND karta = :5 AND bestand = :6 AND delbestand = :7"));
		m_saCommand.Param(1).setAsLong() = ph.ytnr;
		m_saCommand.Param(2).setAsLong() = ph.region;
		m_saCommand.Param(3).setAsLong() = ph.distrikt;
		m_saCommand.Param(4).setAsLong() = ph.lag;
		m_saCommand.Param(5).setAsLong() = ph.karta;
		m_saCommand.Param(6).setAsLong() = ph.bestand;
		m_saCommand.Param(7).setAsLong() = ph.delbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeleteTrakt(TRACTHEADER &th)
{
	// Remove trakt record from db
	try
	{
		m_saCommand.setCommandText(_T("DELETE FROM rojning_trakt WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6"));
		m_saCommand.Param(1).setAsLong() = th.region;
		m_saCommand.Param(2).setAsLong() = th.distrikt;
		m_saCommand.Param(3).setAsLong() = th.lag;
		m_saCommand.Param(4).setAsLong() = th.karta;
		m_saCommand.Param(5).setAsLong() = th.bestand;
		m_saCommand.Param(6).setAsLong() = th.delbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetPlotData(PlotVector &plots, int regionId /*=-1*/, int distriktId /*=-1*/, int lagId /*=-1*/, int karta /*=-1*/, int bestand /*=-1*/, int delbestand /*=-1*/)
{
	PLOTHEADER ph;

	plots.clear();

	try
	{
		// Get all plots or plots under specified trakt
		if( regionId != -1 && distriktId != -1 && lagId != -1 && karta != -1 && bestand != -1 && delbestand != -1 )
		{
			m_saCommand.setCommandText(_T("SELECT * FROM rojning_yta WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6 ORDER BY ytnr"));
			m_saCommand.Param(1).setAsLong() = regionId;
			m_saCommand.Param(2).setAsLong() = distriktId;
			m_saCommand.Param(3).setAsLong() = lagId;
			m_saCommand.Param(4).setAsLong() = karta;
			m_saCommand.Param(5).setAsLong() = bestand;
			m_saCommand.Param(6).setAsLong() = delbestand;
		}
		else
		{
			m_saCommand.setCommandText(_T("SELECT * FROM rojning_yta ORDER BY region, distrikt, lag, karta, ytnr"));
		}
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			ph.ytnr				= m_saCommand.Field("ytnr").asLong();
			ph.region			= m_saCommand.Field("region").asLong();
			ph.distrikt			= m_saCommand.Field("distrikt").asLong();
			ph.lag				= m_saCommand.Field("lag").asLong();
			ph.karta			= m_saCommand.Field("karta").asLong();
			ph.bestand			= m_saCommand.Field("bestand").asLong();
			ph.delbestand		= m_saCommand.Field("delbestand").asLong();
			ph.latitud			= m_saCommand.Field("latitud").asString();
			ph.longitud			= m_saCommand.Field("longitud").asString();
			ph.hansynsyta		= m_saCommand.Field("hansynsyta").asLong();
			ph.orojt			= m_saCommand.Field("orojt").asLong();
			ph.lagskarm			= m_saCommand.Field("lagskarm").asLong();
			ph.totalalder		= m_saCommand.Field("totalalder").asLong();
			ph.vegtyp			= m_saCommand.Field("vegtyp").asLong();
			ph.jordart			= m_saCommand.Field("jordart").asLong();
			ph.markfukt			= m_saCommand.Field("markfukt").asLong();
			ph.si				= m_saCommand.Field("si").asString();
			ph.stam[0]			= m_saCommand.Field("stamTall").asLong();
			ph.stam[1]			= m_saCommand.Field("stamGran").asLong();
			ph.stam[2]			= m_saCommand.Field("stamBjork").asLong();
			ph.stam[3]			= m_saCommand.Field("stamOvlov").asLong();
			ph.stam[4]			= m_saCommand.Field("stamCont").asLong();
			ph.betadStam[0]		= m_saCommand.Field("betadStamTall").asLong();
			ph.betadStam[1]		= m_saCommand.Field("betadStamGran").asLong();
			ph.betadStam[2]		= m_saCommand.Field("betadStamBjork").asLong();
			ph.betadStam[3]		= m_saCommand.Field("betadStamOvlov").asLong();
			ph.betadStam[4]		= m_saCommand.Field("betadStamCont").asLong();
			ph.aterbetadStam[0]	= m_saCommand.Field("aterbetadStamTall").asLong();
			ph.aterbetadStam[1]	= m_saCommand.Field("aterbetadStamGran").asLong();
			ph.aterbetadStam[2]	= m_saCommand.Field("aterbetadStamBjork").asLong();
			ph.aterbetadStam[3]	= m_saCommand.Field("aterbetadStamOvlov").asLong();
			ph.aterbetadStam[4]	= m_saCommand.Field("aterbetadStamCont").asLong();
			ph.stamMhojd[0]		= m_saCommand.Field("stamMhojdTall").asLong();
			ph.stamMhojd[1]		= m_saCommand.Field("stamMhojdGran").asLong();
			ph.stamMhojd[2]		= m_saCommand.Field("stamMhojdBjork").asLong();
			ph.stamMhojd[3]		= m_saCommand.Field("stamMhojdOvlov").asLong();
			ph.stamMhojd[4]		= m_saCommand.Field("stamMhojdCont").asLong();
			ph.kommentar		= m_saCommand.Field("kommentar").asLong();
			ph.rattTrsl			= m_saCommand.Field("rattTrsl").asLong();
			ph.nastaAtgard		= m_saCommand.Field("nastaAtgard").asLong();

			plots.push_back(ph);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTraktData(TractVector &tracts)
{
	TRACTHEADER th;

	tracts.clear();

	try
	{
		m_saCommand.setCommandText(_T("SELECT * FROM rojning_trakt ORDER BY region, distrikt, lag, karta"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			th.region			= m_saCommand.Field("region").asLong();
			th.distrikt			= m_saCommand.Field("distrikt").asLong();
			th.lag				= m_saCommand.Field("lag").asLong();
			th.karta			= m_saCommand.Field("karta").asLong();
			th.bestand			= m_saCommand.Field("bestand").asLong();
			th.delbestand		= m_saCommand.Field("delbestand").asLong();
			th.invtyp			= m_saCommand.Field("invtyp").asLong();
			th.ursprung			= m_saCommand.Field("ursprung").asLong();
			th.user				= m_saCommand.Field("anvandare").asString();
			th.namn				= m_saCommand.Field("namn").asString();
			th.areal			= float(m_saCommand.Field("areal").asDouble());
			th.hoh				= m_saCommand.Field("hoh").asLong();
			th.radie			= float(m_saCommand.Field("radie").asDouble());
			th.stam[0]			= m_saCommand.Field("stamTall").asLong();
			th.stam[1]			= m_saCommand.Field("stamGran").asLong();
			th.stam[2]			= m_saCommand.Field("stamBjork").asLong();
			th.stam[3]			= m_saCommand.Field("stamOvlov").asLong();
			th.stam[4]			= m_saCommand.Field("stamCont").asLong();
			th.stamMhojd[0]		= m_saCommand.Field("stamMhojdTall").asLong();
			th.stamMhojd[1]		= m_saCommand.Field("stamMhojdGran").asLong();
			th.stamMhojd[2]		= m_saCommand.Field("stamMhojdBjork").asLong();
			th.stamMhojd[3]		= m_saCommand.Field("stamMhojdOvlov").asLong();
			th.stamMhojd[4]		= m_saCommand.Field("stamMhojdCont").asLong();
			th.totalalder		= m_saCommand.Field("totalalder").asLong();
			th.vegtyp			= m_saCommand.Field("vegtyp").asLong();
			th.jordart			= m_saCommand.Field("jordart").asLong();
			th.markfukt			= m_saCommand.Field("markfukt").asLong();
			th.si				= m_saCommand.Field("si").asString();
			th.avvikerFranRUS	= m_saCommand.Field("avvikerFranRUS").asLong();
			th.avvikerKommentar	= m_saCommand.Field("avvikerKommentar").asString();
			th.myr				= m_saCommand.Field("myr").asLong();
			th.hallmark			= m_saCommand.Field("hallmark").asLong();
			th.hansynBiotop	= m_saCommand.Field("hansynBiotop").asLong();
			th.lovandel			= m_saCommand.Field("lovandel").asLong();
			th.kulturmiljoer	= m_saCommand.Field("kulturmiljoer").asLong();
			th.fornminnen		= m_saCommand.Field("fornminnen").asLong();
			th.skyddszoner		= m_saCommand.Field("skyddszoner").asLong();
			th.naturvardestrad	= m_saCommand.Field("naturvardestrad").asLong();
			th.nedskrapning		= m_saCommand.Field("nedskrapning").asLong();
			th.fritext			= m_saCommand.Field("fritext").asString();

			// Format bestand (always four digits, may begin with a zero)
			//th.bestand.Format(_T("%.4d"), bestand);

			// Format date
			SADateTime date = m_saCommand.Field(_T("datum")).asDateTime();
			th.datum.Format(_T("%04d-%02d-%02d"), date.GetYear(), date.GetMonth(), date.GetDay());

			tracts.push_back(th);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetDistrictName(int districtId, int regionId, CString &districtName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT district_name FROM district_table WHERE district_num = :1 AND district_region_num = :2"));
		m_saCommand.Param(1).setAsLong() = districtId;
		m_saCommand.Param(2).setAsLong() = regionId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			districtName = m_saCommand.Field("district_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetRegionName(int regionId, CString &regionName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT region_name FROM region_table WHERE region_num = :1"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			regionName = m_saCommand.Field("region_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTeamName(int teamId, int districtId, int regionId, CString &teamName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT team_name FROM team_table WHERE team_region_num = :1 AND team_district_num = :2 AND team_num = :3"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Param(2).setAsLong() = districtId;
		m_saCommand.Param(3).setAsLong() = teamId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			teamName = m_saCommand.Field("team_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetDistrictList(int regionId, IdList &ids)
{
	try
	{
		// Obtain list of all district id:s
		m_saCommand.setCommandText(_T("SELECT district_num FROM district_table WHERE district_region_num = :1 ORDER BY district_num DESC"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTeamList(int districtId, int regionId, IdList &ids)
{
	try
	{
		// Obtain list of all team id:s
		m_saCommand.setCommandText(_T("SELECT team_num FROM team_table WHERE team_region_num = :1 AND team_district_num = :2 ORDER BY team_num DESC"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Param(2).setAsLong() = districtId;
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetRegionList(IdList &ids)
{
	try
	{
		// Obtain list of all region id:s
		m_saCommand.setCommandText(_T("SELECT region_num FROM region_table ORDER BY region_num DESC"));
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::UpdateTrakt(TRACTHEADER &th, int oldRegion, int oldDistrikt, int oldLag, int oldKarta, int oldBestand, int oldDelbestand)
{
	try
	{
		// Update trakt record (note: plot is automatically updated if any primary key value is changed because of the ON UPDATE CASCADE rule)
		m_saCommand.setCommandText( _T("UPDATE rojning_trakt SET region = :1, distrikt = :2, lag = :3, karta = :4, bestand = :5, delbestand = :6, ")
									_T("invtyp = :7, ursprung = :8, anvandare = :9, datum = :10, namn = :11, areal = :12, hoh = :13, radie = :14, ")
									_T("stamTall = :15, stamGran = :16, stamBjork = :17, stamOvlov = :18, stamCont = :19, ")
									_T("stamMhojdTall = :20, stamMhojdGran = :21, stamMhojdBjork = :22, stamMhojdOvlov = :23, stamMhojdCont = :24, ")
									_T("totalalder = :25, vegtyp = :26, jordart = :27, markfukt = :28, si = :29, avvikerFranRUS = :30, avvikerKommentar = :31, ")
									_T("myr = :32, hallmark = :33, hansynBiotop = :34, lovandel = :35, kulturmiljoer = :36, fornminnen = :37, skyddszoner = :38, ")
									_T("naturvardestrad = :39, nedskrapning = :40, fritext = :41 ")
									_T("WHERE region = :42 AND distrikt = :43 AND lag = :44 AND karta = :45 AND bestand = :46 AND delbestand = :47") );
		m_saCommand.Param(1).setAsLong()	= th.region;
		m_saCommand.Param(2).setAsLong()	= th.distrikt;
		m_saCommand.Param(3).setAsLong()	= th.lag;
		m_saCommand.Param(4).setAsLong()	= th.karta;
		m_saCommand.Param(5).setAsLong()	= th.bestand;
		m_saCommand.Param(6).setAsLong()	= th.delbestand;
		m_saCommand.Param(7).setAsLong()	= th.invtyp;
		m_saCommand.Param(8).setAsLong()	= th.ursprung;
		m_saCommand.Param(9).setAsString()	= th.user;
		m_saCommand.Param(10).setAsString()	= th.datum;
		m_saCommand.Param(11).setAsString()	= th.namn;
		m_saCommand.Param(12).setAsDouble()	= th.areal;
		m_saCommand.Param(13).setAsLong()	= th.hoh;
		m_saCommand.Param(14).setAsDouble()	= th.radie;
		m_saCommand.Param(15).setAsLong()	= th.stam[0];
		m_saCommand.Param(16).setAsLong()	= th.stam[1];
		m_saCommand.Param(17).setAsLong()	= th.stam[2];
		m_saCommand.Param(18).setAsLong()	= th.stam[3];
		m_saCommand.Param(19).setAsLong()	= th.stam[4];
		m_saCommand.Param(20).setAsLong()	= th.stamMhojd[0];
		m_saCommand.Param(21).setAsLong()	= th.stamMhojd[1];
		m_saCommand.Param(22).setAsLong()	= th.stamMhojd[2];
		m_saCommand.Param(23).setAsLong()	= th.stamMhojd[3];
		m_saCommand.Param(24).setAsLong()	= th.stamMhojd[4];
		m_saCommand.Param(25).setAsLong()	= th.totalalder;
		m_saCommand.Param(26).setAsLong()	= th.vegtyp;
		m_saCommand.Param(27).setAsLong()	= th.jordart;
		m_saCommand.Param(28).setAsLong()	= th.markfukt;
		m_saCommand.Param(29).setAsString()	= th.si;
		m_saCommand.Param(30).setAsLong()	= th.avvikerFranRUS;
		m_saCommand.Param(31).setAsString()	= th.avvikerKommentar;
		m_saCommand.Param(32).setAsLong()	= th.myr;
		m_saCommand.Param(33).setAsLong()	= th.hallmark;
		m_saCommand.Param(34).setAsLong()	= th.hansynBiotop;
		m_saCommand.Param(35).setAsLong()	= th.lovandel;
		m_saCommand.Param(36).setAsLong()	= th.kulturmiljoer;
		m_saCommand.Param(37).setAsLong()	= th.fornminnen;
		m_saCommand.Param(38).setAsLong()	= th.skyddszoner;
		m_saCommand.Param(39).setAsLong()	= th.naturvardestrad;
		m_saCommand.Param(40).setAsLong()	= th.nedskrapning;
		m_saCommand.Param(41).setAsString()	= th.fritext;
		m_saCommand.Param(42).setAsLong()	= oldRegion;
		m_saCommand.Param(43).setAsLong()	= oldDistrikt;
		m_saCommand.Param(44).setAsLong()	= oldLag;
		m_saCommand.Param(45).setAsLong()	= oldKarta;
		m_saCommand.Param(46).setAsLong()	= oldBestand;
		m_saCommand.Param(47).setAsLong()	= oldDelbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::ShowImportDialog()
{
	bool ret = true;
	const int FILELIST_SIZE = 8192;
	CFileDialog *pFileDlg;
	CString filename, filelist;
	POSITION pos;

	if( !(pFileDlg = new CFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT, _T("XML-filer (*.xml)|*.xml||"))) )
	{
		return false;
	}

	// Show file dialog
	pFileDlg->GetOFN().lpstrFile = filelist.GetBuffer(FILELIST_SIZE);
	pFileDlg->GetOFN().nMaxFile = FILELIST_SIZE;
	if( pFileDlg->DoModal() == IDOK )
	{
		AfxGetApp()->BeginWaitCursor();

		// Import selected files to DB
		pos = pFileDlg->GetStartPosition();
		while( pos )
		{
			filename = pFileDlg->GetNextPathName(pos);
			if( ParseXml(filename) )
			{
				MoveFileToBackupDir(filename);
			}
			else
			{
				ret = false;
				break;
			}
		}

		AfxGetApp()->EndWaitCursor();
	}
	else
	{
		ret = false;
	}

	delete pFileDlg;

	return ret;
}

bool CDBFunc::AddRecord(TRACTHEADER &th)
{
	CString date;
	int plotid = 0;

	FormatDate(th.datum, date);

	// Turn off autocommit in case an error occur
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOff);

	try
	{
		// Make sure this file hasn't been imported already
		m_saCommand.setCommandText( _T("SELECT 1 FROM rojning_trakt WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6") );
		m_saCommand.Param(1).setAsLong()	= th.region;
		m_saCommand.Param(2).setAsLong()	= th.distrikt;
		m_saCommand.Param(3).setAsLong()	= th.lag;
		m_saCommand.Param(4).setAsLong()	= th.karta;
		m_saCommand.Param(5).setAsLong()	= th.bestand;
		m_saCommand.Param(6).setAsLong()	= th.delbestand;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}

			// Record found
			CString str;
			str.Format( _T("Denna trakt finns redan inl�st!\nRegion/distrikt/lag/karta: %d/%d/%d/%d\nBest�nd-delbest�nd: %d-%d"),
						th.region, th.distrikt, th.lag, th.karta, th.bestand, th.delbestand );
			AfxMessageBox(str);
			return false;
		}

		// Add tract record
		m_saCommand.setCommandText( _T("INSERT INTO rojning_trakt (region, distrikt, lag, karta, bestand, delbestand, invtyp, ursprung, anvandare, datum, namn, areal, hoh, radie, stamTall, stamGran, stamBjork, stamOvlov, stamCont, stamMhojdTall, stamMhojdGran, stamMhojdBjork, stamMhojdOvlov, stamMhojdCont, totalalder, vegtyp, jordart, markfukt, si, avvikerFranRUS, avvikerKommentar, myr, hallmark, hansynBiotop, lovandel, kulturmiljoer, fornminnen, skyddszoner, naturvardestrad, nedskrapning, fritext) ")
									_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41)") );
		m_saCommand.Param(1).setAsLong()	= th.region;
		m_saCommand.Param(2).setAsLong()	= th.distrikt;
		m_saCommand.Param(3).setAsLong()	= th.lag;
		m_saCommand.Param(4).setAsLong()	= th.karta;
		m_saCommand.Param(5).setAsLong()	= th.bestand;
		m_saCommand.Param(6).setAsLong()	= th.delbestand;
		m_saCommand.Param(7).setAsLong()	= th.invtyp;
		m_saCommand.Param(8).setAsLong()	= th.ursprung;
		m_saCommand.Param(9).setAsString()	= th.user;
		m_saCommand.Param(10).setAsString()	= th.datum;
		m_saCommand.Param(11).setAsString()	= th.namn;
		m_saCommand.Param(12).setAsDouble()	= th.areal;
		m_saCommand.Param(13).setAsLong()	= th.hoh;
		m_saCommand.Param(14).setAsDouble()	= th.radie;
		m_saCommand.Param(15).setAsLong()	= th.stam[0];
		m_saCommand.Param(16).setAsLong()	= th.stam[1];
		m_saCommand.Param(17).setAsLong()	= th.stam[2];
		m_saCommand.Param(18).setAsLong()	= th.stam[3];
		m_saCommand.Param(19).setAsLong()	= th.stam[4];
		m_saCommand.Param(20).setAsLong()	= th.stamMhojd[0];
		m_saCommand.Param(21).setAsLong()	= th.stamMhojd[1];
		m_saCommand.Param(22).setAsLong()	= th.stamMhojd[2];
		m_saCommand.Param(23).setAsLong()	= th.stamMhojd[3];
		m_saCommand.Param(24).setAsLong()	= th.stamMhojd[4];
		m_saCommand.Param(25).setAsLong()	= th.totalalder;
		m_saCommand.Param(26).setAsLong()	= th.vegtyp;
		m_saCommand.Param(27).setAsLong()	= th.jordart;
		m_saCommand.Param(28).setAsLong()	= th.markfukt;
		m_saCommand.Param(29).setAsString()	= th.si;
		m_saCommand.Param(30).setAsLong()	= th.avvikerFranRUS;
		m_saCommand.Param(31).setAsString()	= th.avvikerKommentar;
		m_saCommand.Param(32).setAsLong()	= th.myr;
		m_saCommand.Param(33).setAsLong()	= th.hallmark;
		m_saCommand.Param(34).setAsLong()	= th.hansynBiotop;
		m_saCommand.Param(35).setAsLong()	= th.lovandel;
		m_saCommand.Param(36).setAsLong()	= th.kulturmiljoer;
		m_saCommand.Param(37).setAsLong()	= th.fornminnen;
		m_saCommand.Param(38).setAsLong()	= th.skyddszoner;
		m_saCommand.Param(39).setAsLong()	= th.naturvardestrad;
		m_saCommand.Param(40).setAsLong()	= th.nedskrapning;
		m_saCommand.Param(41).setAsString()	= th.fritext;		
		m_saCommand.Execute();

		// Add all plots under this tract
		PlotVector::const_iterator iter = th.plots.begin();
		while( iter != th.plots.end() )
		{
			m_saCommand.setCommandText( _T("INSERT INTO rojning_yta (ytnr, region, distrikt, lag, karta, bestand, delbestand, latitud, longitud, hansynsyta, orojt, lagskarm, totalalder, vegtyp, jordart, markfukt, si, stamTall, stamGran, stamBjork, stamOvlov, stamCont, betadStamTall, betadStamGran, betadStamBjork, betadStamOvlov, betadStamCont, aterbetadStamTall, aterbetadStamGran, aterbetadStamBjork, aterbetadStamOvlov, aterbetadStamCont, stamMhojdTall, stamMhojdGran, stamMhojdBjork, stamMhojdOvlov, stamMhojdCont, kommentar, rattTrsl, nastaAtgard) ")
										_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40)") );
			m_saCommand.Param(1).setAsLong()	= iter->ytnr;
			m_saCommand.Param(2).setAsLong()	= th.region;
			m_saCommand.Param(3).setAsLong()	= th.distrikt;
			m_saCommand.Param(4).setAsLong()	= th.lag;
			m_saCommand.Param(5).setAsLong()	= th.karta;
			m_saCommand.Param(6).setAsLong()	= th.bestand;
			m_saCommand.Param(7).setAsLong()	= th.delbestand;
			m_saCommand.Param(8).setAsString()	= iter->latitud;
			m_saCommand.Param(9).setAsString()	= iter->longitud;
			m_saCommand.Param(10).setAsLong()	= iter->hansynsyta;
			m_saCommand.Param(11).setAsLong()	= iter->orojt;
			m_saCommand.Param(12).setAsLong()	= iter->lagskarm;
			m_saCommand.Param(13).setAsLong()	= iter->totalalder;
			m_saCommand.Param(14).setAsLong()	= iter->vegtyp;
			m_saCommand.Param(15).setAsLong()	= iter->jordart;
			m_saCommand.Param(16).setAsLong()	= iter->markfukt;
			m_saCommand.Param(17).setAsString()	= iter->si;
			m_saCommand.Param(18).setAsLong()	= iter->stam[0];
			m_saCommand.Param(19).setAsLong()	= iter->stam[1];
			m_saCommand.Param(20).setAsLong()	= iter->stam[2];
			m_saCommand.Param(21).setAsLong()	= iter->stam[3];
			m_saCommand.Param(22).setAsLong()	= iter->stam[4];
			m_saCommand.Param(23).setAsLong()	= iter->betadStam[0];
			m_saCommand.Param(24).setAsLong()	= iter->betadStam[1];
			m_saCommand.Param(25).setAsLong()	= iter->betadStam[2];
			m_saCommand.Param(26).setAsLong()	= iter->betadStam[3];
			m_saCommand.Param(27).setAsLong()	= iter->betadStam[4];
			m_saCommand.Param(28).setAsLong()	= iter->aterbetadStam[0];
			m_saCommand.Param(29).setAsLong()	= iter->aterbetadStam[1];
			m_saCommand.Param(30).setAsLong()	= iter->aterbetadStam[2];
			m_saCommand.Param(31).setAsLong()	= iter->aterbetadStam[3];
			m_saCommand.Param(32).setAsLong()	= iter->aterbetadStam[4];
			m_saCommand.Param(33).setAsLong()	= iter->stamMhojd[0];
			m_saCommand.Param(34).setAsLong()	= iter->stamMhojd[1];
			m_saCommand.Param(35).setAsLong()	= iter->stamMhojd[2];
			m_saCommand.Param(36).setAsLong()	= iter->stamMhojd[3];
			m_saCommand.Param(37).setAsLong()	= iter->stamMhojd[4];
			m_saCommand.Param(38).setAsLong()	= iter->kommentar;
			m_saCommand.Param(39).setAsLong()	= iter->rattTrsl;
			m_saCommand.Param(40).setAsLong()	= iter->nastaAtgard;
			m_saCommand.Execute();

			iter++;
		}

		// Everything succeeded - commit changes to db
		getDBConnection().conn->Commit();

		// Keep track of last added record
		m_lastTractKey.region		= th.region;
		m_lastTractKey.distrikt		= th.distrikt;
		m_lastTractKey.lag			= th.lag;
		m_lastTractKey.karta		= th.karta;;
		//m_lastTractKey.bestand.Format(_T("%.4d"), _tstoi(th.bestand));		// Always four digits, pad with zeros
		m_lastTractKey.bestand		= th.bestand;
		m_lastTractKey.delbestand	= th.delbestand;
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Turn autocommit back on
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOn);

	return true;
}

bool CDBFunc::ExportRecords(CString dbFile)
{
	SAConnection dbConn;
	SACommand dbCommand;
	SAString connStr;
	SAString str, date;

	AfxGetApp()->BeginWaitCursor();

	try
	{
		connStr.Format(_T("Driver={Microsoft Access Driver (*.mdb)};Dbq=%s;Uid=Admin;Pwd=;"), dbFile.GetString());
		dbConn.Connect(connStr, "", "", SA_ODBC_Client);
		dbCommand.setConnection(&dbConn);

		// Try creating required tables (we cannot check if they already exist so this will throw an exception if it's already created)
		try
		{
			dbCommand.setCommandText( SQLCREATE_TRAKT );
			dbCommand.Execute();
		}
		catch( ... )
		{
		}

		try
		{
			dbCommand.setCommandText( SQLCREATE_YTA );
			dbCommand.Execute();
		}
		catch( ... )
		{
		}

		// Copy data - trakter
		m_saCommand.setCommandText(_T("SELECT * FROM rojning_trakt"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			// Format date
			SADateTime dt = m_saCommand.Field("datum").asDateTime();
			date.Format(_T("%04d-%02d-%02d"), dt.GetYear(), dt.GetMonth(), dt.GetDay());

			// Insert trakt data
			dbCommand.setCommandText( _T("INSERT INTO rojning_trakt (region, distrikt, lag, karta, bestand, delbestand, invtyp, ursprung, anvandare, datum, namn, areal, hoh, radie, stamTall, stamGran, stamBjork, stamOvlov, stamCont, stamMhojdTall, stamMhojdGran, stamMhojdBjork, stamMhojdOvlov, stamMhojdCont, totalalder, vegtyp, jordart, markfukt, si, avvikerFranRUS, avvikerKommentar, myr, hallmark, hansynBiotop, lovandel, kulturmiljoer, fornminnen, skyddszoner, naturvardestrad, nedskrapning, fritext) ")
									  _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41)") );
			dbCommand.Param(1).setAsLong()		= m_saCommand.Field("region").asLong();
			dbCommand.Param(2).setAsLong()		= m_saCommand.Field("distrikt").asLong();
			dbCommand.Param(3).setAsLong()		= m_saCommand.Field("lag").asLong();
			dbCommand.Param(4).setAsLong()		= m_saCommand.Field("karta").asLong();
			dbCommand.Param(5).setAsLong()		= m_saCommand.Field("bestand").asLong();
			dbCommand.Param(6).setAsLong()		= m_saCommand.Field("delbestand").asLong();
			dbCommand.Param(7).setAsLong()		= m_saCommand.Field("invtyp").asLong();
			dbCommand.Param(8).setAsLong()		= m_saCommand.Field("ursprung").asLong();
			dbCommand.Param(9).setAsString()	= m_saCommand.Field("anvandare").asString();
			dbCommand.Param(10).setAsString()	= date;
			dbCommand.Param(11).setAsString()	= m_saCommand.Field("namn").asString();
			dbCommand.Param(12).setAsDouble()	= m_saCommand.Field("areal").asDouble();
			dbCommand.Param(13).setAsLong()		= m_saCommand.Field("hoh").asLong();
			dbCommand.Param(14).setAsDouble()	= m_saCommand.Field("radie").asDouble();
			dbCommand.Param(15).setAsLong()		= m_saCommand.Field("stamTall").asLong();
			dbCommand.Param(16).setAsLong()		= m_saCommand.Field("stamGran").asLong();
			dbCommand.Param(17).setAsLong()		= m_saCommand.Field("stamBjork").asLong();
			dbCommand.Param(18).setAsLong()		= m_saCommand.Field("stamOvlov").asLong();
			dbCommand.Param(19).setAsLong()		= m_saCommand.Field("stamCont").asLong();
			dbCommand.Param(20).setAsLong()		= m_saCommand.Field("stamMhojdTall").asLong();
			dbCommand.Param(21).setAsLong()		= m_saCommand.Field("stamMhojdGran").asLong();
			dbCommand.Param(22).setAsLong()		= m_saCommand.Field("stamMhojdBjork").asLong();
			dbCommand.Param(23).setAsLong()		= m_saCommand.Field("stamMhojdOvlov").asLong();
			dbCommand.Param(24).setAsLong()		= m_saCommand.Field("stamMhojdCont").asLong();
			dbCommand.Param(25).setAsLong()		= m_saCommand.Field("totalalder").asLong();
			dbCommand.Param(26).setAsLong()		= m_saCommand.Field("vegtyp").asLong();
			dbCommand.Param(27).setAsLong()		= m_saCommand.Field("jordart").asLong();
			dbCommand.Param(28).setAsLong()		= m_saCommand.Field("markfukt").asLong();
			dbCommand.Param(29).setAsString()	= m_saCommand.Field("si").asString();
			dbCommand.Param(30).setAsLong()		= m_saCommand.Field("avvikerFranRUS").asLong();
			dbCommand.Param(31).setAsString()	= m_saCommand.Field("avvikerKommentar").asString();
			dbCommand.Param(32).setAsLong()		= m_saCommand.Field("myr").asLong();
			dbCommand.Param(33).setAsLong()		= m_saCommand.Field("hallmark").asLong();
			dbCommand.Param(34).setAsLong()		= m_saCommand.Field("hansynBiotop").asLong();
			dbCommand.Param(35).setAsLong()		= m_saCommand.Field("lovandel").asLong();
			dbCommand.Param(36).setAsLong()		= m_saCommand.Field("kulturmiljoer").asLong();
			dbCommand.Param(37).setAsLong()		= m_saCommand.Field("fornminnen").asLong();
			dbCommand.Param(38).setAsLong()		= m_saCommand.Field("skyddszoner").asLong();
			dbCommand.Param(39).setAsLong()		= m_saCommand.Field("naturvardestrad").asLong();
			dbCommand.Param(40).setAsLong()		= m_saCommand.Field("nedskrapning").asLong();
			dbCommand.Param(41).setAsString()	= m_saCommand.Field("fritext").asString();
			dbCommand.Execute();
		}

		// Ytor
		m_saCommand.setCommandText(_T("SELECT * FROM rojning_yta"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			dbCommand.setCommandText( _T("INSERT INTO rojning_yta (ytnr, region, distrikt, lag, karta, bestand, delbestand, latitud, longitud, hansynsyta, orojt, lagskarm, totalalder, vegtyp, jordart, markfukt, si, stamTall, stamGran, stamBjork, stamOvlov, stamCont, betadStamTall, betadStamGran, betadStamBjork, betadStamOvlov, betadStamCont, aterbetadStamTall, aterbetadStamGran, aterbetadStamBjork, aterbetadStamOvlov, aterbetadStamCont, stamMhojdTall, stamMhojdGran, stamMhojdBjork, stamMhojdOvlov, stamMhojdCont, kommentar, rattTrsl, nastaAtgard) ")
									  _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40)") );
			dbCommand.Param(1).setAsLong()		= m_saCommand.Field("ytnr").asLong();
			dbCommand.Param(2).setAsLong()		= m_saCommand.Field("region").asLong();
			dbCommand.Param(3).setAsLong()		= m_saCommand.Field("distrikt").asLong();
			dbCommand.Param(4).setAsLong()		= m_saCommand.Field("lag").asLong();
			dbCommand.Param(5).setAsLong()		= m_saCommand.Field("karta").asLong();
			dbCommand.Param(6).setAsLong()		= m_saCommand.Field("bestand").asLong();
			dbCommand.Param(7).setAsLong()		= m_saCommand.Field("delbestand").asLong();
			dbCommand.Param(8).setAsString()	= m_saCommand.Field("latitud").asString();
			dbCommand.Param(9).setAsString()	= m_saCommand.Field("longitud").asString();
			dbCommand.Param(10).setAsLong()		= m_saCommand.Field("hansynsyta").asLong();
			dbCommand.Param(11).setAsLong()		= m_saCommand.Field("orojt").asLong();
			dbCommand.Param(12).setAsLong()		= m_saCommand.Field("lagskarm").asLong();
			dbCommand.Param(13).setAsLong()		= m_saCommand.Field("totalalder").asLong();
			dbCommand.Param(14).setAsLong()		= m_saCommand.Field("vegtyp").asLong();
			dbCommand.Param(15).setAsLong()		= m_saCommand.Field("jordart").asLong();
			dbCommand.Param(16).setAsLong()		= m_saCommand.Field("markfukt").asLong();
			dbCommand.Param(17).setAsString()	= m_saCommand.Field("si").asString();
			dbCommand.Param(18).setAsLong()		= m_saCommand.Field("stamTall").asLong();
			dbCommand.Param(19).setAsLong()		= m_saCommand.Field("stamGran").asLong();
			dbCommand.Param(20).setAsLong()		= m_saCommand.Field("stamBjork").asLong();
			dbCommand.Param(21).setAsLong()		= m_saCommand.Field("stamOvlov").asLong();
			dbCommand.Param(22).setAsLong()		= m_saCommand.Field("stamCont").asLong();
			dbCommand.Param(23).setAsLong()		= m_saCommand.Field("betadStamTall").asLong();
			dbCommand.Param(24).setAsLong()		= m_saCommand.Field("betadStamGran").asLong();
			dbCommand.Param(25).setAsLong()		= m_saCommand.Field("betadStamBjork").asLong();
			dbCommand.Param(26).setAsLong()		= m_saCommand.Field("betadStamOvlov").asLong();
			dbCommand.Param(27).setAsLong()		= m_saCommand.Field("betadStamCont").asLong();
			dbCommand.Param(28).setAsLong()		= m_saCommand.Field("aterbetadStamTall").asLong();
			dbCommand.Param(29).setAsLong()		= m_saCommand.Field("aterbetadStamGran").asLong();
			dbCommand.Param(30).setAsLong()		= m_saCommand.Field("aterbetadStamBjork").asLong();
			dbCommand.Param(31).setAsLong()		= m_saCommand.Field("aterbetadStamOvlov").asLong();
			dbCommand.Param(32).setAsLong()		= m_saCommand.Field("aterbetadStamCont").asLong();
			dbCommand.Param(33).setAsLong()		= m_saCommand.Field("stamMhojdTall").asLong();
			dbCommand.Param(34).setAsLong()		= m_saCommand.Field("stamMhojdGran").asLong();
			dbCommand.Param(35).setAsLong()		= m_saCommand.Field("stamMhojdBjork").asLong();
			dbCommand.Param(36).setAsLong()		= m_saCommand.Field("stamMhojdOvlov").asLong();
			dbCommand.Param(37).setAsLong()		= m_saCommand.Field("stamMhojdCont").asLong();
			dbCommand.Param(38).setAsLong()		= m_saCommand.Field("kommentar").asLong();
			dbCommand.Param(39).setAsLong()		= m_saCommand.Field("rattTrsl").asLong();
			dbCommand.Param(40).setAsLong()		= m_saCommand.Field("nastaAtgard").asLong();
			dbCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Make sure temp connection is close before leaving this function
	try
	{
		dbConn.Disconnect();
	}
	catch( ... )
	{
	}

	AfxGetApp()->EndWaitCursor();

	return true;
}

bool CDBFunc::MakeSureTablesExist()
{
	SAString str;

	try
	{
		// Trakt
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'rojning_trakt' AND xtype='U') %s"), SQLCREATE_TRAKT);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Yta
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'rojning_yta' AND xtype='U') %s"), SQLCREATE_YTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();


		// Foreign key constraints
		// Yta - Trakt
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM Information_Schema.Table_Constraints WHERE CONSTRAINT_NAME = 'FK_rojning_yta_rojning_trakt') %s"), SQLCONSTRAINT_YTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();


		// Views (CREATE VIEW must be the first statement in query so we need to check if the view exist prior to executing the CREATE VIEW command)
		m_saCommand.setCommandText(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'rojning' AND xtype='V') SELECT 1"));
		m_saCommand.Execute();
		if( m_saCommand.isResultSet() )
		{
			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}

			m_saCommand.setCommandText(SQLVIEW_ROJNING);
			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::MoveFileToBackupDir(CString &filepath)
{
	// Set up backup directory if not exist
	CString path;
	TCHAR szPath[MAX_PATH];
	if( SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szPath)) )
	{
		path.Format( _T("%s\\%s"), szPath, SUBDIR_HMS );
	}
	else
	{
		return false;
	}

	// Create directory %My documents%\HMS
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Create directory %My documents%\HMS\Data
	path += CString("\\") + SUBDIR_DATA;
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Create directory %My documents%\HMS\Data\R�jning
	path += CString("\\") + SUBDIR_R�JNING;
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Move file to backup dir (append index in case filename already exist; "filename(x).ext")
	CString filename = filepath.Right(filepath.GetLength() - filepath.ReverseFind('\\') - 1);
	CString moveto = path + "\\" + filename;
	CString movetoex = moveto;
	CString filenum;
	int i = 1;
	while( !(MoveFileEx(filepath, movetoex, MOVEFILE_COPY_ALLOWED | MOVEFILE_WRITE_THROUGH)) ) // Enable move from network device
	{
		// Only run this loop in case we need to change the filename
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			CString str; str.Format(_T("Misslyckades att flytta '%s' till '%s'!"), filename, movetoex);
			AfxMessageBox(str);
			break;
		}

		filenum.Format( _T("(%d)"), i++ );
		movetoex = moveto;
		movetoex.Insert( movetoex.ReverseFind('.'), filenum );
	}

	return true;
}

bool CDBFunc::ParseXml(CString &filename)
{
	USES_CONVERSION;
	TiXmlNode *pNode, *pRootNode;
#ifdef UNICODE
	TiXmlDocument doc( W2A(filename) );
#else
	TiXmlDocument doc( filename );
#endif

	// Load xml file
	if( !doc.LoadFile() )
	{
		return false;
	}

	// Get root node
	pRootNode = doc.FirstChild("XML");
	if( !pRootNode )
	{
		return false;
	}

	// Find all 'Trakt' elements
	pNode = pRootNode->FirstChild("Trakt");
	while( pNode )
	{
		CString file;
		TCHAR fn[260], ext[260];
		_tsplitpath(filename, NULL, NULL, fn, ext);
		file.Format(_T("%s%s"), fn, ext);
		if( !ParseTrakt(pNode, file) )
		{
			return false;
		}
		pNode = pNode->NextSibling();
	}

	return true;
}

bool CDBFunc::ParseTrakt(TiXmlNode *pNode, CString &filename)
{
	int plotnum = 0; // Counter for plots under in this tract
	TRACTHEADER th;
	TiXmlElement *pElem;

	pElem = pNode->FirstChildElement();
	if( !pElem )
	{
		return false;
	}
	while( pElem )
	{
		// Get all elements under each 'Yta'
		if( strcmp(pElem->Value(), "Yta") == 0 )
		{
			if( !ParseYta(pElem, th, ++plotnum) )
				return false;
		}
		else
		{
			// Store values (use lower-case to make case-insensitive)
			char szValue[256]; strcpy(szValue, pElem->Value()); _strlwr(szValue);
			if( strcmp(szValue, "slutf�rd") == 0 )
			{
				// Check if file is finished
				if( atoi(pElem->GetText()) == 0 )
				{
					CString str; str.Format(_T("Denna trakt '%s' �r inte slutf�rd. Vill du l�sa in den �nd�?"), filename);
					if( AfxMessageBox(str, MB_YESNO | MB_ICONEXCLAMATION) == IDNO )
					{
						return false;
					}
				}
			}
			else if( strcmp(szValue, "grundf�rh�llande") == 0 )
			{
				// MbPl - wrong application for this file
				CString str; str.Format(_T("Denna trakt '%s' tillh�r MbPl och kan inte l�sas in till r�jningen!"), filename);
				AfxMessageBox(str);
				return false;
			}
			else if( strcmp(szValue, "invtyp") == 0 )
			{
				th.invtyp = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "ursprung") == 0 )
			{
				th.ursprung = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "lag") == 0 )
			{
				th.lag = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "region") == 0 )
			{
				th.region = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "distrikt") == 0 )
			{
				th.distrikt = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "user") == 0 )
			{
				th.user = pElem->GetText();
			}
			else if( strcmp(szValue, "datum") == 0 )
			{
				th.datum = pElem->GetText();
			}
			else if( strcmp(szValue, "karta") == 0 )
			{
				th.karta = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "best�nd") == 0 && th.ursprung != 2 || strcmp(szValue, "traktnummer") == 0 && th.ursprung == 2 )
			{
				th.bestand = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "delbest�nd") == 0 )
			{
				th.delbestand = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "best�ndsnamn") == 0 && th.ursprung != 2 || strcmp(szValue, "traktnamn") == 0 && th.ursprung == 2 )
			{
				th.namn = pElem->GetText();
			}
			else if( strcmp(szValue, "areal") == 0 )
			{
				CString str = pElem->GetText();
				str.Replace('.', ',');
				th.areal = float(_tstof(str));
			}
			else if( strcmp(szValue, "h�h") == 0 )
			{
				th.hoh = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "radie") == 0 )
			{
				CString str = pElem->GetText();
				str.Replace('.', ',');
				th.radie = float(_tstof(str));
			}
			else if( strcmp(szValue, "stamtall") == 0 )
			{
				th.stam[0] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stamgran") == 0 )
			{
				th.stam[1] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stambj�rk") == 0 )
			{
				th.stam[2] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stam�vl�v") == 0 )
			{
				th.stam[3] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stamcont") == 0 )
			{
				th.stam[4] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stammhojdtall") == 0 )
			{
				th.stamMhojd[0] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stammhojdgran") == 0 )
			{
				th.stamMhojd[1] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stammhojdbj�rk") == 0 )
			{
				th.stamMhojd[2] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stammhojd�vl�v") == 0 )
			{
				th.stamMhojd[3] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "stammhojdcont") == 0 )
			{
				th.stamMhojd[4] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "total�lder") == 0 )
			{
				th.totalalder = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "vegtyp") == 0 )
			{
				th.vegtyp = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "jordart") == 0 )
			{
				th.jordart = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "markfukt") == 0 )
			{
				th.markfukt = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "si") == 0 )
			{
				th.si = pElem->GetText();
			}
			else if( strcmp(szValue, "avvikerfr�nrus") == 0 )
			{
				th.avvikerFranRUS = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "avvikerkommentar") == 0 )
			{
				th.avvikerKommentar = pElem->GetText();
			}
			else if( strcmp(szValue, "myr") == 0 )
			{
				th.myr = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "h�llmark") == 0 )
			{
				th.hallmark = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "h�nsynbiotop") == 0 )
			{
				th.hansynBiotop = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "l�vandel") == 0 )
			{
				th.lovandel = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "kulturmilj�er") == 0 )
			{
				th.kulturmiljoer = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "fornminnen") == 0 )
			{
				th.fornminnen = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "skyddszoner") == 0 )
			{
				th.skyddszoner = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "naturv�rdestr�d") == 0 )
			{
				th.naturvardestrad = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "nedskr�pning") == 0 )
			{
				th.nedskrapning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "fritext") == 0 )
			{
				th.fritext = pElem->GetText();
			}
		}

		pElem = pElem->NextSiblingElement();
	}

	// Ber�kna medelh�jd stam (saknas i fil)
	int sum[5]; memset(&sum, 0, sizeof(sum));
	PlotVector::const_iterator iter = th.plots.begin();
	while( iter != th.plots.end() )
	{
		for( int i = 0; i < 5; i++ )
		{
			th.stamMhojd[i] += iter->stamMhojd[i] * iter->stam[i];
			sum[i] += iter->stam[i];
		}
		iter++;
	}
	for( int i = 0; i < 5; i++ )
	{
		if( sum[i] > 0 )
		{
			th.stamMhojd[i] /= sum[i];
		}
	}

	AddRecord(th);

	return true;
}

bool CDBFunc::ParseYta(TiXmlNode *pNode, TRACTHEADER &th, int plotnum)
{
	int ytid = 0;
	PLOTHEADER ph;
	TiXmlElement *pElem;

	ph.ytnr = plotnum;

	// Get all elements under this node
	pElem = pNode->FirstChildElement();
	if( !pElem )
	{
		return false;
	}
	while( pElem )
	{
		// Store values (use lower-case to make case-insensitive)
		char szValue[256]; strcpy(szValue, pElem->Value()); _strlwr(szValue);
		if( strcmp(szValue, "latitud") == 0 )
		{
			ph.latitud = pElem->GetText();
		}
		else if( strcmp(szValue, "longitud") == 0 )
		{
			ph.longitud = pElem->GetText();
		}
		else if( strcmp(szValue, "h�nsynsyta") == 0 )
		{
			ph.hansynsyta = atoi(pElem->GetText()); 
		}
		else if( strcmp(szValue, "or�jt") == 0 )
		{
			ph.orojt = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "l�gsk�rm") == 0 )
		{
			ph.lagskarm = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "total�lder") == 0 )
		{
			ph.totalalder = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "vegtyp") == 0 )
		{
			ph.vegtyp = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "jordart") == 0 )
		{
			ph.jordart = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "markfukt") == 0 )
		{
			ph.markfukt = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "si") == 0 )
		{
			ph.si = pElem->GetText();
		}
		else if( strcmp(szValue, "stamtall") == 0 )
		{
			ph.stam[0] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stamgran") == 0 )
		{
			ph.stam[1] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stambj�rk") == 0 )
		{
			ph.stam[2] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stam�vl�v") == 0 )
		{
			ph.stam[3] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stamcont") == 0 )
		{
			ph.stam[4] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "betadstamtall") == 0 )
		{
			ph.betadStam[0] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "betadstamgran") == 0 )
		{
			ph.betadStam[1] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "betadstambj�rk") == 0 )
		{
			ph.betadStam[2] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "betadstam�vl�v") == 0 )
		{
			ph.betadStam[3] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "betadstamcont") == 0 )
		{
			ph.betadStam[4] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "�terbetadstamtall") == 0 )
		{
			ph.aterbetadStam[0] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "�terbetadstamgran") == 0 )
		{
			ph.aterbetadStam[1] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "�terbetadstambj�rk") == 0 )
		{
			ph.aterbetadStam[2] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "�terbetadstam�vl�v") == 0 )
		{
			ph.aterbetadStam[3] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "�terbetadstamcont") == 0 )
		{
			ph.aterbetadStam[4] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stammhojdtall") == 0 )
		{
			ph.stamMhojd[0] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stammhojdgran") == 0 )
		{
			ph.stamMhojd[1] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stammhojdbj�rk") == 0 )
		{
			ph.stamMhojd[2] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stammhojd�vl�v") == 0 )
		{
			ph.stamMhojd[3] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "stammhojdcont") == 0 )
		{
			ph.stamMhojd[4] = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "kommentar") == 0 )
		{
			ph.kommentar = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "r�tttrsl") == 0 )
		{
			ph.rattTrsl = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "n�sta�tg�rd") == 0 )
		{
			ph.nastaAtgard = atoi(pElem->GetText());
		}

		pElem = pElem->NextSiblingElement();
	}

	ph.ytnr = int(th.plots.size()) + 1;
	th.plots.push_back(ph);

	return true;
}

// Helper function used to format date string from yyyymmdd to yyyy-mm-dd
bool CDBFunc::FormatDate(CString str, CString &date)
{
	TCHAR y[5], m[3], d[3];

	if( _tcslen(str) == 10 && str[4] == '-' && str[7] == '-' )
	{
		// String is already formatted
		date = str;
		return true;
	}

	if( _tcslen(str) != 8 )
	{
		AfxMessageBox(_T("Ok�nt datumformat. Ange datum p� formen ����mmdd."));
		return false;
	}

	// Extract date parts
	memcpy( y, (TCHAR*)(str.GetBuffer() + 0), 4 * sizeof(TCHAR) ); y[4] = '\0';
	memcpy( m, (TCHAR*)(str.GetBuffer() + 4), 2 * sizeof(TCHAR) ); m[2] = '\0';
	memcpy( d, (TCHAR*)(str.GetBuffer() + 6), 2 * sizeof(TCHAR) ); d[2] = '\0';

	date.Format( _T("%s-%s-%s"), y, m, d );

	return true;
}

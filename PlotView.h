#pragma once

#include "DBFunc.h"


// CPlotView form view

class CPlotView : public CFormView
{
	DECLARE_DYNCREATE(CPlotView)

protected:
	CPlotView();
	virtual ~CPlotView();

	void DeletePlot();

	CDBFunc *m_pDB;
	int m_nDBIndex;
	int m_nPlantIndex;
	PlotVector m_plotData;

public:
	void PopulateData(UINT idx = 0);
	void ReloadPlotData(bool bUpdateToolbar = true);
	void SetupToolbarButtons();

	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMPLOT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg int  OnCreate(LPCREATESTRUCT lpcs);
	afx_msg BOOL OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pCopyDataStruct);
	DECLARE_MESSAGE_MAP()

	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);

	CMyExtEdit m_region;
	CMyExtEdit m_distrikt;
	CMyExtEdit m_lag;
	CMyExtEdit m_karta;
	CMyExtEdit m_bestand;
	CMyExtEdit m_delbestand;
	CMyExtEdit m_ytnr;
	CMyExtEdit m_latitud;
	CMyExtEdit m_longitud;
	CMyComboBox m_hansynsyta;
	CMyComboBox m_orojt;
	CMyComboBox m_lagskarm;
	CMyExtEdit m_totalalder;
	CMyComboBox m_vegtyp;
	CMyComboBox m_jordart;
	CMyComboBox m_markfukt;
	CMyExtEdit m_si;
	CMyComboBox m_kommentar;
	CMyComboBox m_rattTrsl;
	CMyComboBox m_nastaAtgard;
	CMyExtEdit m_stam[5];
	CMyExtEdit m_betadStam[5];
	CMyExtEdit m_aterbetadStam[5];
	CMyExtEdit m_stamMhojd[5];
};

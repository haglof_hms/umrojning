CREATE TABLE [rojning_trakt]
(
	[region] [int] NOT NULL,
	[distrikt] [int] NOT NULL,
	[lag] [int] NOT NULL,
	[karta] [int] NOT NULL,
	[bestand] [smallint] NOT NULL,
	[delbestand] [smallint] NOT NULL,
	[invtyp] [smallint] NOT NULL,
	[ursprung] [smallint] NOT NULL,
	[anvandare] [varchar](50) NULL,
	[datum] [smalldatetime] NOT NULL,
	[namn] [varchar](50) NULL,
	[areal] [float] NULL,
	[hoh] [smallint] NOT NULL,
	[radie] [float] NOT NULL,
	[stamTall] [smallint] NULL,
	[stamGran] [smallint] NULL,
	[stamBjork] [smallint] NULL,
	[stamOvlov] [smallint] NULL,
	[stamCont] [smallint] NULL,
	[stamMhojdTall] [smallint] NULL,
	[stamMhojdGran] [smallint] NULL,
	[stamMhojdBjork] [smallint] NULL,
	[stamMhojdOvlov] [smallint] NULL,
	[stamMhojdCont] [smallint] NULL,
	[totalalder] [smallint] NULL,
	[vegtyp] [smallint] NULL,
	[jordart] [smallint] NULL,
	[markfukt] [smallint] NULL,
	[si] [varchar](5) NULL,
	[avvikerFranRUS] [smallint] NULL,
	[avvikerKommentar] [varchar](200) NULL,
	[myr] [smallint] NULL,
	[hallmark] [smallint] NULL,
	[hansynBiotop] [smallint] NULL,
	[lovandel] [smallint] NULL,
	[kulturmiljoer] [smallint] NULL,
	[fornminnen] [smallint] NULL,
	[skyddszoner] [smallint] NULL,
	[naturvardestrad] [smallint] NULL,
	[nedskrapning] [smallint] NULL,
	[fritext] [varchar](200) NULL,
	CONSTRAINT [PK_rojning_trakt] PRIMARY KEY CLUSTERED
	(
		[region] ASC,
		[distrikt] ASC,
		[lag] ASC,
		[karta] ASC,
		[bestand] ASC,
		[delbestand] ASC
	)
)


CREATE TABLE [rojning_yta]
(
	[ytnr] [smallint] NOT NULL,
	[region] [int] NOT NULL,
	[distrikt] [int] NOT NULL,
	[lag] [int] NOT NULL,
	[karta] [int] NOT NULL,
	[bestand] [smallint] NOT NULL,
	[delbestand] [smallint] NOT NULL,
	[latitud] [varchar](10) NULL,
	[longitud] [varchar](10) NULL,
	[hansynsyta] [smallint] NULL,
	[orojt] [smallint] NULL,
	[lagskarm] [smallint] NULL,
	[totalalder] [smallint] NULL,
	[vegtyp] [smallint] NULL,
	[jordart] [smallint] NULL,
	[markfukt] [smallint] NULL,
	[si] [varchar](5) NULL,
	[stamTall] [smallint] NULL,
	[stamGran] [smallint] NULL,
	[stamBjork] [smallint] NULL,
	[stamOvlov] [smallint] NULL,
	[stamCont] [smallint] NULL,
	[betadStamTall] [smallint] NULL,
	[betadStamGran] [smallint] NULL,
	[betadStamBjork] [smallint] NULL,
	[betadStamOvlov] [smallint] NULL,
	[betadStamCont] [smallint] NULL,
	[aterbetadStamTall] [smallint] NULL,
	[aterbetadStamGran] [smallint] NULL,
	[aterbetadStamBjork] [smallint] NULL,
	[aterbetadStamOvlov] [smallint] NULL,
	[aterbetadStamCont] [smallint] NULL,
	[stamMhojdTall] [smallint] NULL,
	[stamMhojdGran] [smallint] NULL,
	[stamMhojdBjork] [smallint] NULL,
	[stamMhojdOvlov] [smallint] NULL,
	[stamMhojdCont] [smallint] NULL,
	[kommentar] [smallint] NULL,
	[rattTrsl] [smallint] NULL,
	[nastaAtgard] [smallint] NULL,
	CONSTRAINT [PK_rojning_yta] PRIMARY KEY CLUSTERED
	(
		[ytnr] ASC,
		[region] ASC,
		[distrikt] ASC,
		[lag] ASC,
		[karta] ASC,
		[bestand] ASC,
		[delbestand] ASC
	)
)


ALTER TABLE [rojning_yta] ADD CONSTRAINT [FK_rojning_yta_rojning_trakt] FOREIGN KEY([region], [distrikt], [lag], [karta], [bestand], [delbestand])
REFERENCES [rojning_trakt] ([region], [distrikt], [lag], [karta], [bestand], [delbestand])
ON UPDATE CASCADE
ON DELETE CASCADE


CREATE VIEW rojning AS
SELECT * FROM 
( 
SELECT antalTall, antalGran, antalBjork, antalOvlov, antalCont, hojdTall, hojdGran, hojdBjork, hojdOvlov, hojdCont, antalTotalt, r, areal a, radie*radie*PI() ytAreal, orojt, region rg, distrikt di, lag lg, karta ka, bestand bd, delbestand db, traktnamn, 
		ROUND(CAST((antalBjork + antalOvlov) AS float) / (antalTall + antalGran + antalBjork + antalOvlov + antalCont) * 100, 0) la, 
		s, 
		(SELECT COUNT(*) FROM rojning_yta y WHERE y.region = dt.region AND y.distrikt = dt.distrikt AND y.lag = dt.lag AND y.karta = dt.karta AND y.bestand = dt.bestand AND y.delbestand = dt.delbestand AND y.orojt = dt.orojt) antalYtor, 
		(SELECT SUM(CAST( 
		CASE WHEN 
			((y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) / (radie*radie*PI()) * 10000) > s - 500 
			AND 
			((y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) / (radie*radie*PI()) * 10000) < s + 500 
		THEN 
			1 
		ELSE 
			0 
		END 
		AS float))
		FROM (SELECT y.stamTall, y.stamGran, y.stamBjork, y.stamOvlov, y.stamCont, radie, 
					CASE WHEN (SELECT ROUND(CAST(SUM(stamBjork + stamOvlov) AS float) / SUM(stamTall + stamGran + stamBjork + stamOvlov + stamCont) * 100, 0) FROM rojning_yta WHERE region = y.region AND distrikt = y.distrikt AND lag = y.lag AND karta = y.karta AND bestand = y.bestand AND delbestand = y.delbestand) > 69 THEN
					( 
						CASE 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 1300 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 1400 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 1500 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 1600 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 1700 
						END 
					) 
					ELSE 
					( 
						CASE 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 2100 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 2200 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 2300 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 2400 
						WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 2500 
						END 
					) 
					END s 
		FROM rojning_yta y INNER JOIN rojning_trakt t ON t.region = y.region AND t.distrikt = y.distrikt AND t.lag = y.lag AND t.karta = y.karta AND t.bestand = y.bestand AND t.delbestand = y.delbestand 
		WHERE y.region = dt.region AND y.distrikt = dt.distrikt AND y.lag = dt.lag AND y.karta = dt.karta AND y.bestand = dt.bestand AND y.delbestand = dt.delbestand AND y.orojt = dt.orojt) y) antalGodkanda
FROM 
( 
	SELECT 
	CASE WHEN (SELECT ROUND(CAST(SUM(stamBjork + stamOvlov) AS float) / SUM(stamTall + stamGran + stamBjork + stamOvlov + stamCont) * 100, 0) FROM rojning_yta WHERE region = y.region AND distrikt = y.distrikt AND lag = y.lag AND karta = y.karta AND bestand = y.bestand AND delbestand = y.delbestand) > 69 THEN
	( 
		CASE 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 1300 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 1400 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 1500 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 1600 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 1700 
		END 
	) 
	ELSE 
	( 
		CASE 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) < 19 THEN 2100 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 19 THEN 2200 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) >= 20 AND RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) <= 22 THEN 2300 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) = 23 THEN 2400 
		WHEN RIGHT(t.si, REPLACE(LEN(t.si)-1, -1, 0)) > 23 THEN 2500 
		END 
	) 
	END s, 
	SUM(y.stamTall) antalTall, SUM(y.stamMhojdTall * y.stamTall) hojdTall, 
	SUM(y.stamGran) antalGran, SUM(y.stamMhojdGran * y.stamGran) hojdGran, 
	SUM(y.stamBjork) antalBjork, SUM(y.stamMhojdBjork * y.stamBjork) hojdBjork, 
	SUM(y.stamOvlov) antalOvlov, SUM(y.stamMhojdOvlov * y.stamOvlov) hojdOvlov, 
	SUM(y.stamCont) antalCont, SUM(y.stamMhojdCont * y.stamCont) hojdCont, 
	SUM(y.stamTall + y.stamGran + y.stamBjork + y.stamOvlov + y.stamCont) antalTotalt, 
	CASE WHEN (y.region = 11 OR y.region = 16) AND y.distrikt <> 21 THEN 5 ELSE 10 END r, 
	(t.areal / (SELECT COUNT(*) FROM rojning_yta y1 WHERE y1.region = y.region AND y1.distrikt = y.distrikt AND y1.lag = y.lag AND y1.karta = y.karta AND y1.bestand = y.bestand AND y1.delbestand = y.delbestand) * COUNT(*)) areal,
	radie,
	orojt, y.region, y.distrikt, y.lag, y.karta, y.bestand, y.delbestand, 
	namn traktnamn
	FROM rojning_yta AS y 
	INNER JOIN 
	rojning_trakt AS t ON t.region = y.region AND t.distrikt = y.distrikt AND t.lag = y.lag AND t.karta = y.karta AND t.bestand = y.bestand AND t.delbestand = y.delbestand
	GROUP BY y.region, y.distrikt, y.lag, y.karta, y.bestand, y.delbestand, namn, orojt, areal, radie, t.si 
) dt 
) dt2 
INNER JOIN rojning_trakt AS t ON t.region = rg AND t.distrikt = di AND t.lag = lg AND t.karta = ka AND t.bestand = bd AND t.delbestand = db

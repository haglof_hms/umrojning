#pragma once

#ifdef UNICODE
	#undef UNICODE
	#include "tinyxml.h"
	#define UNICODE
#else
	#include "tinyxml.h"
#endif

#include <DBBaseClass_SQLApi.h>
#include <SQLAPI.h>
#include <list>
#include <vector>


class PLOTHEADER
{
public:
	PLOTHEADER() { Reset(); }
	~PLOTHEADER() {}

	// Nyckelvariabler
	int ytnr;
	int region;
	int distrikt;
	int lag;
	int karta;
	int bestand;
	int delbestand;

	CString latitud;
	CString longitud;
	int hansynsyta;
	int orojt;
	int lagskarm;
	int totalalder;
	int vegtyp;
	int jordart;
	int markfukt;
	CString si;
	int stam[5];
	int betadStam[5];
	int aterbetadStam[5];
	int stamMhojd[5];
	int kommentar;
	int rattTrsl;
	int nastaAtgard;

	void Reset()
	{
		ytnr		= 0;
		region		= 0;
		distrikt	= 0;
		lag			= 0;
		karta		= 0;
		bestand		= 0;
		delbestand	= 0;

		latitud.Empty();
		longitud.Empty();
		hansynsyta	= -1;
		orojt		= -1;
		lagskarm	= -1;
		totalalder	= 0;
		vegtyp		= -1;
		jordart		= -1;
		markfukt	= -1;
		si.Empty();
		memset(stam, 0, sizeof(stam));
		memset(betadStam, 0, sizeof(betadStam));
		memset(aterbetadStam, 0, sizeof(aterbetadStam));
		memset(stamMhojd, 0, sizeof(stamMhojd));
		kommentar	= -1;
		rattTrsl	= -1;
		nastaAtgard	= -1;
	}
};

typedef std::vector<PLOTHEADER> PlotVector;


struct TRACTHEADER
{
	TRACTHEADER() { Reset(); }
	~TRACTHEADER() {}

	bool slutford;		// Slutförd (intern flagga)

	int invtyp;			// Inventeringstyp (egen, disrikts, etc.)
	int ursprung;		// Ursprung (egen skog, rotköp, etc.)
	int lag;
	int region;
	int distrikt;
	CString user;
	CString datum;
	int karta;
	int bestand;		// Bestånd/traktnummer
	int delbestand;
	CString namn;		// Beståndsnamn/traktnamn
	float areal;
	int hoh;
	float radie;
	int stam[5];
	int stamMhojd[5];
	int totalalder;
	int vegtyp;
	int jordart;
	int markfukt;
	CString si;			// Ståndortsindex
	int avvikerFranRUS;
	CString avvikerKommentar;
	int myr;
	int hallmark;		// Hällmark
	int hansynBiotop;	// Hänsynskrävande biotoper
	int lovandel;
	int kulturmiljoer;
	int fornminnen;
	int skyddszoner;
	int naturvardestrad;
	int nedskrapning;
	CString fritext;

	PlotVector plots;

	void Reset()
	{
		slutford		= false;
		invtyp			= -1;
		ursprung		= -1;
		lag				= 0;
		region			= 0;
		distrikt		= 0;
		user.Empty();
		datum.Empty();
		karta			= 0;
		bestand			= 0;
		delbestand		= 0;
		namn.Empty();
		areal			= 0;
		hoh				= 0;
		radie			= 0;
		memset(stam, 0, sizeof(stam));
		memset(stamMhojd, 0, sizeof(stamMhojd));
		totalalder		= 0;
		vegtyp			= -1;
		jordart			= -1;
		markfukt		= -1;
		si.Empty();
		avvikerFranRUS	= -1;
		avvikerKommentar.Empty();
		myr				= -1;
		hallmark		= -1;
		hansynBiotop	= -1;
		lovandel		= -1;
		kulturmiljoer	= -1;
		fornminnen		= -1;
		skyddszoner		= -1;
		naturvardestrad	= -1;
		nedskrapning	= -1;
		fritext.Empty();
	}
};

typedef std::vector<TRACTHEADER> TractVector;


typedef std::list<int> IdList;


class CDBFunc : public CDBBaseClass_SQLApi
{
public:
	CDBFunc( DB_CONNECTION_DATA &condata );
	~CDBFunc();

	bool AddRecord(TRACTHEADER &th);
	bool ExportRecords(CString dbFile);
	bool DeletePlot(PLOTHEADER &ph);
	bool DeleteTrakt(TRACTHEADER &th);
	bool GetPlotData(PlotVector &plots, int regionId = -1, int distriktId = -1, int lagId = -1, int karta = -1, int bestand = -1, int delbestand = -1);
	bool GetTraktData(TractVector &tracts);
	bool GetDistrictName(int districtId, int regionId, CString &districtName);
	bool GetRegionName(int regionId, CString &regionName);
	bool GetTeamName(int teamId, int districtId, int regionId, CString &teamName);
	bool GetDistrictList(int regionId, IdList &ids);
	bool GetTeamList(int districtId, int regionId, IdList &ids);
	bool GetRegionList(IdList &ids);
	bool MakeSureTablesExist();
	bool UpdateTrakt(TRACTHEADER &th, int oldRegion, int oldDistrikt, int oldLag, int oldKarta, int oldBestand, int oldDelbestand);
	bool ShowImportDialog();

	const TRACTHEADER& GetLastTractKey() { return m_lastTractKey; }

	static void setupForDBConnection( HWND hWndReciv, HWND hWndSend );

protected:
	bool MoveFileToBackupDir(CString &filepath);

	bool ParseXml(CString &filename);
	bool ParseTrakt(TiXmlNode *pNode, CString &filename);
	bool ParseYta(TiXmlNode *pNode, TRACTHEADER &th, int plotnum);
	bool ParsePlanta(TiXmlNode *pNode, PLOTHEADER &ph);

	bool FormatDate(CString str, CString &date);

	TRACTHEADER m_lastTractKey; // Hold key values for last added tract (used to bring up last added record after import & reload is finished)
};

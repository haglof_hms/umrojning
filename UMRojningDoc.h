#pragma once

#include "stdafx.h"

class CRöjningDoc : public CDocument
{
protected: // create from serialization only
	CRöjningDoc();
	virtual ~CRöjningDoc();

	DECLARE_DYNCREATE(CRöjningDoc)

// Generated message map functions
protected:
	//{{AFX_MSG(CRöjningDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#pragma once

class CPlotFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CPlotFrame)

public:
	CPlotFrame();
	virtual ~CPlotFrame();

protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	bool m_bOnce;
	CString m_sLangFN;
};

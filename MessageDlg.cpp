// MessageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MessageDlg.h"


// CMessageDialog dialog

IMPLEMENT_DYNAMIC(CMessageDialog, CDialog)

BEGIN_MESSAGE_MAP(CMessageDialog, CDialog)
END_MESSAGE_MAP()

CMessageDialog::CMessageDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageDialog::IDD, pParent)
{
}

CMessageDialog::CMessageDialog(LPCTSTR cap, LPCTSTR ok_btn, LPCTSTR cancel_btn, LPCTSTR msg, CWnd *pParent /*=NULL*/)
	: CDialog(CMessageDialog::IDD, pParent)
{
	m_sCaption = cap;
	m_sOKBtn = ok_btn;
	m_sCancelBtn = cancel_btn;
	m_sMsgText = msg;
}

CMessageDialog::~CMessageDialog()
{
}

void CMessageDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_HTML_TEXT, m_wndHTML);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CMessageDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_sCaption);

	m_wndHTML.SetWindowText(m_sMsgText);
	m_wndHTML.ModifyStyleEx(0, WS_EX_TRANSPARENT);

	m_wndOKBtn.SetWindowText(m_sOKBtn);
	m_wndCancelBtn.SetWindowText(m_sCancelBtn);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


// CMessageDialog message handlers

#include "stdafx.h"
#include "DBFunc.h"
#include "MessageDlg.h"
#include "UMRojningGenerics.h"
#include "UMRojningStrings.h"
#include "PlotSelectList.h"
#include "PlotView.h"
#include "TraktSelectList.h"
#include "TraktView.h"


// CTraktView

IMPLEMENT_DYNCREATE(CTraktView, CFormView)

BEGIN_MESSAGE_MAP(CTraktView, CFormView)
	//{{AFX_MSG_MAP(CTraktView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_CBN_SELCHANGE(IDC_COMBO1, &OnCbnSelchangeRegion)
	ON_CBN_SELCHANGE(IDC_COMBO2, &OnCbnSelchangeDistrict)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CTraktView::CTraktView():
CFormView(CTraktView::IDD),
m_bAdding(false),
m_pDB(NULL),
m_nDBIndex(0)
{
}

CTraktView::~CTraktView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CTraktView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_region);
	DDX_Control(pDX, IDC_COMBO2, m_distrikt);
	DDX_Control(pDX, IDC_COMBO30, m_lag);
	DDX_Control(pDX, IDC_EDIT8, m_karta);
	DDX_Control(pDX, IDC_EDIT16, m_areal);
	DDX_Control(pDX, IDC_COMBO4, m_invtyp);
	DDX_Control(pDX, IDC_COMBO5, m_ursprung);
	DDX_Control(pDX, IDC_EDIT15, m_anvandare);
	DDX_Control(pDX, IDC_EDIT18, m_hoh);
	DDX_Control(pDX, IDC_EDIT6, m_bestand);
	DDX_Control(pDX, IDC_EDIT7, m_delbestand);
	DDX_Control(pDX, IDC_EDIT3, m_traktnamn);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_datum);
	DDX_Control(pDX, IDC_EDIT19, m_radie);
	DDX_Control(pDX, IDC_COMBO10, m_jordart);
	DDX_Control(pDX, IDC_EDIT22, m_si);
	DDX_Control(pDX, IDC_COMBO20, m_skyddszoner);
	DDX_Control(pDX, IDC_COMBO21, m_naturvardestrad);
	DDX_Control(pDX, IDC_COMBO26, m_nedskrapning);
	DDX_Control(pDX, IDC_COMBO32, m_fornminnen);
	DDX_Control(pDX, IDC_EDIT23, m_fritext);
	DDX_Control(pDX, IDC_EDIT33, m_totalalder);
	DDX_Control(pDX, IDC_COMBO9, m_vegtyp);
	DDX_Control(pDX, IDC_COMBO31, m_kulturmiljo);
	DDX_Control(pDX, IDC_COMBO16, m_myr);
	DDX_Control(pDX, IDC_COMBO17, m_hallmark);
	DDX_Control(pDX, IDC_COMBO11, m_markfukt);
	DDX_Control(pDX, IDC_COMBO12, m_avvikerFranRUS);
	DDX_Control(pDX, IDC_EDIT34, m_avvikerKommentar);
	DDX_Control(pDX, IDC_COMBO18, m_hansynBiotop);
	DDX_Control(pDX, IDC_COMBO22, m_lovandel);
	DDX_Control(pDX, IDC_EDIT40, m_stam[0]);
	DDX_Control(pDX, IDC_EDIT55, m_stam[1]);
	DDX_Control(pDX, IDC_EDIT56, m_stam[2]);
	DDX_Control(pDX, IDC_EDIT44, m_stam[3]);
	DDX_Control(pDX, IDC_EDIT45, m_stam[4]);
	DDX_Control(pDX, IDC_EDIT46, m_stamMhojd[0]);
	DDX_Control(pDX, IDC_EDIT47, m_stamMhojd[1]);
	DDX_Control(pDX, IDC_EDIT48, m_stamMhojd[2]);
	DDX_Control(pDX, IDC_EDIT49, m_stamMhojd[3]);
	DDX_Control(pDX, IDC_EDIT50, m_stamMhojd[4]);
}

void CTraktView::PopulateData(int idx /*=-1*/)
{
	int i;
	CString str, tmp;
	TRACTHEADER *th;

	// Update index if specified (if we aren't adding a record)
	if( idx >= 0 && !m_bAdding )
		m_nDBIndex = idx;

	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_traktData.size() && !m_bAdding )
	{
		th = &m_traktData[m_nDBIndex];

		// List districts, teams under this region, district
		ListDistricts(th->region);
		ListTeams(th->region, th->distrikt);

		// Load cached data
		m_region.SetCurSel(-1);
		for( i = 0; i < m_region.GetCount(); i++ )
		{
			if( m_region.GetItemData(i) == th->region )
			{
				m_region.SetCurSel(i);
			}
		}

		m_distrikt.SetCurSel(-1);
		for( i = 0; i < m_distrikt.GetCount(); i++ )
		{
			if( m_distrikt.GetItemData(i) == th->distrikt )
			{
				m_distrikt.SetCurSel(i);
			}
		}

		m_lag.SetCurSel(-1);
		for( i = 0; i < m_lag.GetCount(); i++ )
		{
			if( m_lag.GetItemData(i) == th->lag )
			{
				m_lag.SetCurSel(i);
			}
		}

		if( th->areal > 0 )
		{
			m_areal.setFloat(th->areal, 1);
		}
		else
		{
			m_areal.SetWindowText(_T(""));
		}

		if( th->hoh > 0 )
		{
			m_hoh.setInt(th->hoh);
		}
		else
		{
			m_hoh.SetWindowText(_T(""));
		}

		if( th->radie > 0 )
		{
			m_radie.setFloat(th->radie, 2);
		}
		else
		{
			m_radie.SetWindowText(_T(""));
		}

		if( th->bestand >= 0 )
		{
			// Best�nd, 4 digits
			CString str; str.Format(_T("%04d"), th->bestand);
			m_bestand.SetWindowText(str);
		}
		else
		{
			m_bestand.SetWindowText(_T(""));
		}

		if( th->delbestand > 0 )
		{
			m_delbestand.setInt(th->delbestand);
		}
		else if( th->delbestand == 0 )
		{
			m_delbestand.SetWindowText(_T("0"));
		}
		else
		{
			m_delbestand.SetWindowText(_T(""));
		}

		// Datum
		COleDateTime oleTime;
		try
		{
			if( oleTime.ParseDateTime(th->datum) )
			{
				m_datum.SetTime(oleTime);
			}
		}
		catch(...)
		{}

		// Jordart
		if( th->jordart == -1 )
		{
			m_jordart.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(JORDART_INDICES) / sizeof(int); i++ )
			{
				if( th->jordart == JORDART_INDICES[i] )
				{
					m_jordart.SetCurSel(i);
				}
			}
		}

		// Naturv�rdes-/d�da tr�d
		if( th->naturvardestrad == -1 )
		{
			m_naturvardestrad.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(NATURVARDESTRAD_INDICES) / sizeof(int); i++ )
			{
				if( th->naturvardestrad == NATURVARDESTRAD_INDICES[i] )
				{
					m_naturvardestrad.SetCurSel(i);
				}
			}
		}

		// Nedskr�pning
		if( th->nedskrapning == -1 )
		{
			m_nedskrapning.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(NEDSKRAPNING_INDICES) / sizeof(int); i++ )
			{
				if( th->nedskrapning == NEDSKRAPNING_INDICES[i] )
				{
					m_nedskrapning.SetCurSel(i);
				}
			}
		}

		// Fornminnen
		if( th->fornminnen == -1 )
		{
			m_fornminnen.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(FORNMINNEN_INDICES) / sizeof(int); i++ )
			{
				if( th->fornminnen == FORNMINNEN_INDICES[i] )
				{
					m_fornminnen.SetCurSel(i);
				}
			}
		}

		// Veg.typ
		if( th->vegtyp == -1 )
		{
			m_vegtyp.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(VEGTYP_INDICES) / sizeof(int); i++ )
			{
				if( th->vegtyp == VEGTYP_INDICES[i] )
				{
					m_vegtyp.SetCurSel(i);
				}
			}
		}

		// Kulturl�mning
		if( th->kulturmiljoer == -1 )
		{
			m_kulturmiljo.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(KULTURLAMNING_INDICES) / sizeof(int); i++ )
			{
				if( th->kulturmiljoer == KULTURLAMNING_INDICES[i] )
				{
					m_kulturmiljo.SetCurSel(i);
				}
			}
		}

		// Myr
		if( th->myr == -1 )
		{
			m_myr.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(MYR_INDICES) / sizeof(int); i++ )
			{
				if( th->myr == MYR_INDICES[i] )
				{
					m_myr.SetCurSel(i);
				}
			}
		}

		// H�llmark
		if( th->hallmark == -1 )
		{
			m_hallmark.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < sizeof(HALLMARK_INDICES) / sizeof(int); i++ )
			{
				if( th->hallmark == HALLMARK_INDICES[i] )
				{
					m_hallmark.SetCurSel(i);
				}
			}
		}

		m_invtyp.SetCurSel(MAX(th->invtyp - 1, -1));
		m_ursprung.SetCurSel(MAX(th->ursprung - 1, -1));
		m_karta.setInt(th->karta);
		m_skyddszoner.SetCurSel(th->skyddszoner);

		m_anvandare.SetWindowText(th->user);
		m_traktnamn.SetWindowText(th->namn);
		m_si.SetWindowText(th->si);
		m_fritext.SetWindowText(th->fritext);
		m_totalalder.setInt(th->totalalder);
		m_markfukt.SetCurSel(th->markfukt);
		m_avvikerFranRUS.SetCurSel(th->avvikerFranRUS);
		m_avvikerKommentar.SetWindowText(th->avvikerKommentar);
		m_hansynBiotop.SetCurSel(th->hansynBiotop);
		m_lovandel.SetCurSel(th->lovandel);

		for( i = 0; i < 5; i++ )
		{
			m_stam[i].setInt(th->stam[i]);
			m_stamMhojd[i].setInt(th->stamMhojd[i]);
		}

		// Enable/disable fields
		EnableFields();
	}
	else
	{
		// Clear fields
		m_region.SetWindowText(_T(""));
		m_distrikt.SetWindowText(_T(""));
		m_lag.SetWindowText(_T(""));
		m_karta.SetWindowText(_T(""));
		m_areal.SetWindowText(_T(""));
		m_hoh.SetWindowText(_T(""));
		m_delbestand.SetWindowText(_T(""));
		m_radie.SetWindowText(_T(""));
		m_anvandare.SetWindowText(_T(""));
		m_bestand.SetWindowText(_T(""));
		m_traktnamn.SetWindowText(_T(""));
		m_region.SetWindowText(_T(""));
		m_si.SetWindowText(_T(""));
		m_fritext.SetWindowText(_T(""));
		m_totalalder.SetWindowText(_T(""));

		m_jordart.SetCurSel(-1);
		m_naturvardestrad.SetCurSel(-1);
		m_nedskrapning.SetCurSel(-1);
		m_fornminnen.SetCurSel(-1);
		m_vegtyp.SetCurSel(-1);
		m_kulturmiljo.SetCurSel(-1);
		m_myr.SetCurSel(-1);
		m_hallmark.SetCurSel(-1);
		m_invtyp.SetCurSel(-1);
		m_ursprung.SetCurSel(-1);
		m_skyddszoner.SetCurSel(-1);
		m_markfukt.SetCurSel(-1);
		m_avvikerFranRUS.SetCurSel(-1);
		m_avvikerKommentar.SetWindowText(_T(""));
		m_hansynBiotop.SetCurSel(-1);
		m_lovandel.SetCurSel(-1);

		for( i = 0; i < 5; i++ )
		{
			m_stam[i].SetWindowText(_T(""));
			m_stamMhojd[i].SetWindowText(_T(""));
		}

		CTime curTime = CTime::GetCurrentTime();
		m_datum.SetTime(&curTime);
	}
}

void CTraktView::BeginAdding()
{
	m_karta.SetReadOnly(FALSE);
	m_bestand.SetReadOnly(FALSE);
	m_delbestand.SetReadOnly(FALSE);

	m_bAdding = true;
}

void CTraktView::EndAdding()
{
	if( m_bAdding )
	{
		EnableFields();
	}
	m_bAdding = false;
}

void CTraktView::DeleteTrakt()
{
	CString msg;
	TRACTHEADER &th = m_traktData[m_nDBIndex];

	// If we're adding a record that hasn't been saved yet -- just reload trakt data to get rid of it
	if( m_bAdding )
	{
		EndAdding();
		ReloadTraktData();
		return;
	}

	// Make sure record exist
	if( m_nDBIndex >= (int)m_traktData.size() )
		return;

	// Bring up confirm dialog
	msg.Format( _T("<center><font size=\"+4\"><b>Ta bort trakt</b></font></center><br><hr><br>")
				_T("Region: <b>%d</b><br>Distrikt: <b>%d</b><br>Lag: <b>%d</b><br>Karta: <b>%d</b><br>Best�nd: <b>%d</b><br>Delbest�nd: <b>%d</b><br>Datum: <b>%s</b><br><hr><br>")
				_T("OBS! Om trakten tas bort kommer underliggande ytor ocks� att raderas.<br><br>")
				_T("<center><font color=\"red\" size=\"+4\">Vill du ta bort denna trakt?</font></center>"),
				th.region, th.distrikt, th.lag, th.karta, th.bestand, th.delbestand, th.datum );
	if( messageDialog(_T("Ta bort trakt"), _T("Ta bort"), _T("Avbryt"), msg) )
	{
		// OK - go on remove trakt
		if( m_pDB->DeleteTrakt(th) )
		{
			ReloadTraktData();
		}
	}
}

void CTraktView::SaveTrakt()
{
	COleDateTime oleTime;
	CString date;
	TRACTHEADER *th;

	// Make sure all required fields are filled before saving
	if( m_region.GetCurSel() == -1  || m_distrikt.GetCurSel() == -1 || m_lag.GetCurSel() == -1 || m_karta.getText().IsEmpty() || (m_ursprung.GetCurSel() != 0 && (m_bestand.getText().IsEmpty() || m_delbestand.getText().IsEmpty())) )
	{
		AfxMessageBox(_T("Du m�ste ange samtliga nyckelv�rden innan trakten kan sparas!\n\nSe till att region, distrikt, lag, karta/traktnummer, best�nd och delbest�nd �r angivet."));
		return;
	}

	// Save current trakt
	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_traktData.size() )
	{
		th = &m_traktData[m_nDBIndex];

		m_datum.GetTime(oleTime);
		date.Format(_T("%04d-%02d-%02d"), oleTime.GetYear(), oleTime.GetMonth(), oleTime.GetDay());

		th->areal		= float(m_areal.getFloat());
		th->hoh			= m_hoh.getInt();
		th->radie		= float(m_radie.getFloat());
		th->datum		= date;

		if( !m_delbestand.getText().IsEmpty() )		th->delbestand	= m_delbestand.getInt();
		else										th->delbestand	= -1;

		if( m_jordart.GetCurSel() != CB_ERR )		th->jordart		= JORDART_INDICES[m_jordart.GetCurSel()];
		else										th->jordart		= -1;

		if( m_naturvardestrad.GetCurSel() != CB_ERR )	th->naturvardestrad	= NATURVARDESTRAD_INDICES[m_naturvardestrad.GetCurSel()];
		else										th->naturvardestrad	= -1;

		if( m_nedskrapning.GetCurSel() != CB_ERR )	th->nedskrapning= NEDSKRAPNING_INDICES[m_nedskrapning.GetCurSel()];
		else										th->nedskrapning= -1;

		if( m_fornminnen.GetCurSel() != CB_ERR )	th->fornminnen	= FORNMINNEN_INDICES[m_fornminnen.GetCurSel()];
		else										th->fornminnen	= -1;

		if( m_vegtyp.GetCurSel() != CB_ERR )		th->vegtyp		= VEGTYP_INDICES[m_vegtyp.GetCurSel()];
		else										th->vegtyp		= -1;

		if( m_kulturmiljo.GetCurSel() != CB_ERR )	th->kulturmiljoer	= KULTURLAMNING_INDICES[m_kulturmiljo.GetCurSel()];
		else										th->kulturmiljoer	= -1;

		if( m_myr.GetCurSel() != CB_ERR )			th->myr			= MYR_INDICES[m_myr.GetCurSel()];
		else										th->myr			= -1;

		if( m_hallmark.GetCurSel() != CB_ERR )		th->hallmark	= HALLMARK_INDICES[m_hallmark.GetCurSel()];
		else										th->hallmark	= -1;

		th->karta			= m_karta.getInt();
		th->invtyp			= m_invtyp.GetCurSel() + 1;
		th->ursprung		= m_ursprung.GetCurSel() + 1;
		th->skyddszoner		= m_skyddszoner.GetCurSel();
		th->user			= m_anvandare.getText();
		th->bestand			= m_bestand.getInt();
		th->namn			= m_traktnamn.getText();
		th->si				= m_si.getText();
		th->fritext			= m_fritext.getText();
		th->totalalder		= m_totalalder.getInt();
		th->markfukt		= m_markfukt.GetCurSel();
		th->avvikerFranRUS	= m_avvikerFranRUS.GetCurSel();
		th->avvikerKommentar= m_avvikerKommentar.getText();
		th->hansynBiotop	= m_hansynBiotop.GetCurSel();
		th->lovandel		= m_lovandel.GetCurSel();

		for( int i = 0; i < 5; i++ )
		{
			th->stam[i] = m_stam[i].getInt();
			th->stamMhojd[i] = m_stamMhojd[i].getInt();
		}

		// Copy key vars
		int oldRegion		= th->region;
		int oldDistrikt		= th->distrikt;
		int oldLag			= th->lag;
		int oldKarta		= th->karta;
		int oldBestand		= th->bestand;
		int oldDelbestand	= th->delbestand;

		// Set/Update key vars (do not update region, distrikt if not set/no changes have been made)
		th->region		= int(m_region.GetItemData(m_region.GetCurSel()));
		th->distrikt	= int(m_distrikt.GetItemData(m_distrikt.GetCurSel()));
		th->lag			= int(m_lag.GetItemData(m_lag.GetCurSel()));
		th->karta		= m_karta.getInt();

		// Add or update record
		if( m_bAdding )
		{
			if( !m_pDB->AddRecord(*th) )
			{
				ReloadTraktData();
			}
		}
		else
		{
			if( !m_pDB->UpdateTrakt(*th, oldRegion, oldDistrikt, oldLag, oldKarta, oldBestand, oldDelbestand) )
			{
				ReloadTraktData();
			}
			else
			{
				// Update average value for this record (we must fill tractheader with belonging plot data before proceeding)
				m_pDB->GetPlotData(th->plots, th->region, th->distrikt, th->lag, th->karta, th->bestand, th->delbestand);
			}
		}

		EnableFields();
	}

	EndAdding();
}

// Reloads trakt data.
// Note: this function should only be called when changes have been made to the database.
void CTraktView::ReloadTraktData( bool bDataAdded /*=false*/ )
{
	if( !m_pDB ) return;

	// End adding so populate is done correctly in case we get here because of an error
	EndAdding();

	// Update trakt data
	m_pDB->GetTraktData( m_traktData );

	// Check if data was added
	if( bDataAdded )
	{
		// Jump to last record (just in case record cannot be found)
		m_nDBIndex = (UINT)(m_traktData.size() - 1);

		// Find index of last added record
		const TRACTHEADER &th = m_pDB->GetLastTractKey();
		for( unsigned int i = 0; i < m_traktData.size(); i++ )
		{
			if( m_traktData[i].region		== th.region &&
				m_traktData[i].distrikt		== th.distrikt &&
				m_traktData[i].lag			== th.lag &&
				m_traktData[i].karta		== th.karta &&
				m_traktData[i].bestand		== th.bestand &&
				m_traktData[i].delbestand	== th.delbestand )
			{
				m_nDBIndex = i;
				break;
			}
		}
	}
	else
	{
		// Data removed - revert to previous record
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;
	}

	PopulateData( m_nDBIndex );
	SetupToolbarButtons();

	// Update trakt list in case window is open
	CTraktSelectList *pTSL = (CTraktSelectList*)getFormViewByID( IDD_TRAKTSELECT );
	if( pTSL )
	{
		pTSL->PopulateReport();
	}

	// Update plot list view
	CPlotSelectList *pPSL = (CPlotSelectList*)getFormViewByID( IDD_PLOTSELECT );
	if( pPSL )
	{
		pPSL->PopulateReport();
	}

	// Update plot view
	CPlotView *pPV = (CPlotView*)getFormViewByID( IDD_FORMPLOT );
	if( pPV )
	{
		pPV->ReloadPlotData( false );
	}
}

void CTraktView::OnInitialUpdate()
{
	CString str, tmp;
	IdList ids;
	IdList::const_iterator iter;
	int i, idx;

	CFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	// Check if we have a valid license
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H9002"), _T(""), _T(""), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);
	if( _tcscmp(msg.getSuite(), _T("0")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}


	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();

	// Fill up combo boxes with possible values
	m_pDB->GetRegionList(ids);
	iter = ids.begin();
	while( iter != ids.end() )
	{
		m_pDB->GetRegionName(*iter, tmp);
		str.Format(_T("%d - %s"), *iter, tmp);
		idx = m_region.AddString(str);
		m_region.SetItemData(idx, *iter); // Store id for each string value
		iter++;
	}

	for( i = 0; i < INVTYP_CT; i++ )
	{
		m_invtyp.AddString(INVTYP[i]);
	}
	for( i = 0; i < URSPRUNG_CT; i++ )
	{
		m_ursprung.AddString(URSPRUNG[i]);
	}
	for( i = 0; i < VEGTYP_CT; i++ )
	{
		m_vegtyp.AddString(VEGTYP[i]);
	}
	for( i = 0; i < JORDART_CT; i++ )
	{
		m_jordart.AddString(JORDART[i]);
	}
	for( i = 0; i < MYR_CT ; i++ )
	{
		m_myr.AddString(MYR[i]);
	}
	for( i = 0; i < HALLMARK_CT ; i++ )
	{
		m_hallmark.AddString(HALLMARK[i]);
	}
	for( i = 0; i < FORNMINNEN_CT ; i++ )
	{
		m_fornminnen.AddString(FORNMINNEN[i]);
	}
	for( i = 0; i < KULTURLAMNING_CT ; i++ )
	{
		m_kulturmiljo.AddString(KULTURLAMNING[i]);
	}
	for( i = 0; i < SKYDDSZONER_CT ; i++ )
	{
		m_skyddszoner.AddString(SKYDDSZONER[i]);
	}
	for( i = 0; i < NATURVARDESTRAD_CT ; i++ )
	{
		m_naturvardestrad.AddString(NATURVARDESTRAD[i]);
	}
	for( i = 0; i < NEDSKRAPNING_CT ; i++ )
	{
		m_nedskrapning.AddString(NEDSKRAPNING[i]);
	}
	for( i = 0; i < MARKFUKT_CT; i++ )
	{
		m_markfukt.AddString(MARKFUKT[i]);
	}
	for( i = 0; i < NEJJA_CT; i++ )
	{
		m_avvikerFranRUS.AddString(NEJJA[i]);
	}
	for( i = 0; i < HANSYNBIOTOP_CT; i++ )
	{
		m_hansynBiotop.AddString(HANSYNBIOTOP[i]);
	}
	for( i = 0; i < LOVANDEL_CT; i++ )
	{
		m_lovandel.AddString(LOVANDEL[i]);
	}

	// Set disabled color for fields
	m_karta.SetDisabledColor(BLACK, INFOBK);
	m_bestand.SetDisabledColor(BLACK, INFOBK);
	m_delbestand.SetDisabledColor(BLACK, INFOBK);

	// Cache trakt data from db
	if( m_pDB )
	{
		m_pDB->GetTraktData( m_traktData );
	}

	PopulateData( m_nDBIndex );
}

void CTraktView::SetupToolbarButtons()
{
	BOOL startPrev, endNext;

	// Enable/disable db navig buttons depending on where in the list we are
	if( m_traktData.size() <= 1 )
	{
		// Only one record or less
		startPrev = FALSE;
		endNext = FALSE;
	}
	else if( m_nDBIndex <= 0 )
	{
		// At the beginning
		startPrev = FALSE;
		endNext = TRUE;
	}
	else if( m_nDBIndex >= ((int)m_traktData.size() - 1) )
	{
		// At the end
		startPrev = TRUE;
		endNext = FALSE;
	}
	else
	{
		// In the middle somewhere
		startPrev = TRUE;
		endNext = TRUE;
	}

	// DB naviagation
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	// Tools
	BOOL itemsLeft = m_traktData.size() ? TRUE : FALSE; // enable only if we have any records
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,itemsLeft);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,itemsLeft);
}

void CTraktView::EnableFields()
{
	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_traktData.size() )
	{
		if( m_traktData[m_nDBIndex].ursprung == 1 )
		{
			m_bestand.SetReadOnly(TRUE);
			m_delbestand.SetReadOnly(TRUE);
		}
		else
		{
			m_bestand.SetReadOnly(FALSE);
			m_delbestand.SetReadOnly(FALSE);
		}

		m_karta.SetReadOnly(TRUE);
	}
}

void CTraktView::ListDistricts(int regionId)
{
	CString str, tmp;
	IdList ids;
	IdList::const_iterator iter;
	int idx;

	// Update list of available districts
	m_distrikt.ResetContent();

	m_pDB->GetDistrictList(regionId, ids);
	iter = ids.begin();
	while( iter != ids.end() )
	{
		m_pDB->GetDistrictName(*iter, regionId, tmp);
		str.Format(_T("%d - %s"), *iter, tmp);
		idx = m_distrikt.AddString(str);
		m_distrikt.SetItemData(idx, *iter);
		iter++;
	}
}

void CTraktView::ListTeams(int regionId, int districtId)
{
	CString str, tmp;
	IdList ids;
	IdList::const_iterator iter;
	int idx;

	// Update list of available teams
	m_lag.ResetContent();

	m_pDB->GetTeamList(districtId, regionId, ids);
	iter = ids.begin();
	while( iter != ids.end() )
	{
		m_pDB->GetTeamName(*iter, districtId, regionId, tmp);
		str.Format(_T("%d - %s"), *iter, tmp);
		idx = m_lag.AddString(str);
		m_lag.SetItemData(idx, *iter);
		iter++;
	}
}

void CTraktView::OnCbnSelchangeDistrict()
{
	m_lag.SetCurSel(-1);
	ListTeams(int(m_region.GetItemData(m_region.GetCurSel())), int(m_distrikt.GetItemData(m_distrikt.GetCurSel())));
}

void CTraktView::OnCbnSelchangeRegion()
{
	m_distrikt.SetCurSel(-1);
	m_lag.SetCurSel(-1);
	ListDistricts(int(m_region.GetItemData(m_region.GetCurSel())));
	ListTeams(int(m_region.GetItemData(m_region.GetCurSel())), int(m_distrikt.GetItemData(m_distrikt.GetCurSel())));
}

BOOL CTraktView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CTraktView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}


LRESULT CTraktView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	TRACTHEADER th;

	if( m_bAdding && (wParam == ID_DBNAVIG_START || wParam == ID_DBNAVIG_END || wParam == ID_DBNAVIG_PREV || wParam == ID_DBNAVIG_NEXT) )
	{
		AfxMessageBox(_T("Du m�ste antingen spara eller ta bort den aktuella trakten innan du kan forts�tta."), MB_OK | MB_ICONINFORMATION);
		return 0;
	}

	switch(wParam)
	{
	case ID_NEW_ITEM:
		m_traktData.push_back(th);
		m_nDBIndex = (int)m_traktData.size() - 1;
		BeginAdding();

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_OPEN_ITEM:
		if( m_pDB->ShowImportDialog() )
		{
			ReloadTraktData(true);
		}
		break;

	case ID_SAVE_ITEM:
		SaveTrakt();
		break;

	case ID_DELETE_ITEM:
		DeleteTrakt();
		break;

	case ID_DBNAVIG_START:
		m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_PREV:
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_NEXT:
		if( ++m_nDBIndex > ((int)m_traktData.size() - 1) )
			m_nDBIndex = (int)m_traktData.size() - 1;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_END:
		m_nDBIndex = ((int)m_traktData.size() - 1);

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;
	};

	return 0;
}
